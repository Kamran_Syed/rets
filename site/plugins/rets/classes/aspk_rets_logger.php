<?php
if ( !class_exists('Aspk_Rets_Logger')){
	class Aspk_Rets_Logger{
		private $path;
		
		protected function __construct(){
			
			$this->path = ABSPATH .'wp-content/uploads/rets_fmls.log';
			
		}
		
		public static function getInstance(){
			static $instance = null;
			if (null === $instance) {
				$instance = new static();
			}

			return $instance;
		}
		
		public function log_post(&$p, $num_results){
			
			$line = date('m/d/YY h:i:s')." Results={$num_results} "; 
			
			foreach($p as $k=>$v){
				
				if($k == 'advanced_open' || $k == 'aspk_search' ) continue;
				$line .= " {$k}={$v} "; 
			}
			
			$line .= PHP_EOL;
			
			$this->write_log($line);
			
		}
		
		private function write_log($line){
			
			file_put_contents($this->path, $line, FILE_APPEND);
			
		}
		
		private function __clone(){
			
		}
		
		private function __wakeup(){
			
		}
		
	}
}	
