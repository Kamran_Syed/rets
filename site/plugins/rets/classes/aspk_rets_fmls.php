<?php
if ( !class_exists('Aspk_Rets_Fmls')){
	class Aspk_Rets_Fmls extends Aspk_Rets_Base {
		protected $end_point;
		protected $userid;
		protected $password;
		protected $allowed_fields;
		protected $search_result_fields;
		protected $detail_result_fields;
		
		function __construct(){
			$this->set_allowed_fields();
			$this->set_search_result_fields();
			$this->set_detail_result_fields();
		}
		
		
		protected function set_allowed_fields(){
			$this->allowed_fields = array("Acres","AgeDesc","AllowAddressDisplayYN","AllowInternetDisplayYN","AlsoListedinFMLSForSaleYN","Amenities","AnnualAssocFee","AnnualExpensesAmt","AnnualMasterAssocFee","AnnualMasterAssocFeeDesc","AnnualUtilitiesAmt","ApplianceDesc","ApplicationFeePerAdultYN","ApplicationFeePerFamilyYN","ApproximateLotSize","Architecture","AssessmentDueContemplatedYN","AssocFeeDesc","AssocFeeIncludes","AssumableYN","Availability","BasementDesc","BathroomDescription","BathsFull","BathsHalf","BedroomDescription","BedsTotal","Block","Boathouse","BuildingFaces","CapRate","CertProfHomeBldrs","City","CoListAgent_MUI","CoListAgentDirectWorkPhone","CoListAgentFullName","CoListAgentMLSID","CoListOffice_MUI","CoListOfficeMLSID","CoListOfficeName","CoListOfficePhone","ComplexBuildingAccess","ConstructionDesc","CoolingDescription","CorrectionCount","CountyOrParish","CPHBStatus","CreditReportRequiredYN","CurrentClassification","CurrentPrice","CurrentUse","CurrentZoning","Deposit","DiningRoomDescription","Directions","DisabilityAccess","District","Dock","DocumentsatOffice","DuplicateMLSNumber","DwellingRooms","ElementarySchool","EnergyFeatures","Exterior","FireplaceFeaturesLocation","FireplacesNum","FloorDescription","FurnishedYN","GreenBuildingCertifications","GrossExpensesAmt","GrossIncomeAmt","HeatType","HERSIndexRating","HERSIndexRatingAvailableYN","HighSchool","HOARentRestrictionsYN","HomeWarranty","HouseFaces","HouseYN","Improvements","InitiationFee","Interior","KitchenFeatures","LakeName","LandlordExpenses","LandlordUtilities","Landlot","LastChangeTimestamp","LastChangeType","LastListPrice","LastStatus","LaundryFeaturesLocation","LeaseTerm","ListAgent_MUI","ListAgentDirectWorkPhone","ListAgentFullName","ListAgentMLSID","ListFirmCode","ListOffice_MUI","ListOfficeMLSID","ListOfficeName","ListOfficePhone","ListPrice","LoadingDesc","LocationDesc","Lot","LotDesc","LotDimensions","LowerBedrooms","LowerFullBaths","LowerHalfBaths","MainBedrooms","MainDwellingStories","MainFullBaths","MainHalfBaths","Management","MasterBathFeatures","Matrix_Unique_ID","MatrixModifiedDT","MiddleSchool","Miscellaneous","MiscellaneousEquipment","MLSNumber","MonthlyAssocFee","MoveInFeeAmt","MoveInFeeYN","MoveOutFeeAmt","MoveOutFeeYN","NeighborhoodAmenities","NOIAmt","NumBuildings","NumofUnits","NumParkingSpaces","OfficeListWeb","OnSiteUtilities","OpenHouseCount","OriginalListPrice","OwnerFinancingYN","OwnerSecondYN","Ownership","ParcelNumber","ParkingDesc","PerAdultAmt","PerFamilyAmt","PetDepositAmt","PetsAllowed","PhotoCount","PhotoModificationTimestamp","PlatBook","PlatPage","PoolonProperty","PossibleFinancingDesc","PossibleUse","PostalCode","PostalCodePlus4","PresentUse","PriceAcreAmt","PriceChangeTimestamp","PropertySubType","PropertySubTypeCommercial","PropertyType","ProposedFinancing","ProviderKey","ProviderModificationTimestamp","PublicRemarksConsumerView","RATIO_CurrentPrice_By_SQFT","RecommendedZoning","ReservationDeposit","RestrictionsRequirements","RoadFrontFeet","RoadSurface","RoadType","RoofType","RoomsDescription","SaleIncludes","SaleOptions","SchoolBusRouteElemYN","SchoolBusRouteHighYN","SchoolBusRouteMiddleYN","SearchArea","SectionGMD","Setting","SewerDesc","SpecialCircumstances","SqFtSource","SqFtTotal","StateOrProvince","Status","StatusChangeTimestamp","StorageFacility","Stories","StreetDirPrefix","StreetDirSuffix","StreetName","StreetNumber","StreetNumberNumeric","StreetSuffix","Style","SubdComplex","SwimTennisFee","SwimTennisFeeDue","Taxes","TaxYear","TenantPaysAssociationFeeYN","TenantPaysCableYN","TenantPaysElectricYN","TenantPaysGarbageYN","TenantPaysGasYN","TenantPaysLawnMaintYN","TenantPaysPestControlYN","TenantPaysSecuritySystemYN","TenantPaysTelephoneYN","TenantPaysWaterYN","TennisonPropertyYN","Timber","Topography","TransactionType","Transportation","UnitCount","UnitFaces","UnitLevels","UnitLocationDesc","UnitNumber","UpperBedrooms","UpperFullBaths","UpperHalfBaths","UtilitiesAvailable","Vegetation","VirtualTourLink","VirtualTourLinkPP","WaterfrontFootage","WaterOnLand","WaterSource","YearBuilt");
		}
		
		protected function set_search_result_fields($fields = false){
			if(is_array($fields)){
				
				$this->search_result_fields = array();
				
				foreach($fields as $f){
					if($this->validate_field($f)){
						$this->search_result_fields[] = $f;
					}
				}
				
			}else{
				$this->search_result_fields = array('BathsFull','BathsHalf','City','LocationDesc','PostalCode','CurrentPrice','PropertyType','MLSNumber','PropertySubType','Matrix_Unique_ID','PhotoCount','UnitLocationDesc','BedroomDescription','Amenities','ConstructionDesc','Exterior','FloorDescription','LoadingDesc','LotDesc','RoomsDescription','UtilitiesAvailable','BedsTotal','PublicRemarksConsumerView','StreetNumber','StreetDirPrefix','StreetName','StreetSuffix','StreetDirSuffix','UnitNumber','CountyOrParish','BedsTotal','SubdComplex','Directions','SqFtTotal','SqFtSource','Interior','MasterBathFeatures','BathroomDescription','KitchenFeatures','DiningRoomDescription','LaundryFeaturesLocation','BasementDesc','DisabilityAccess','FireplacesNum','CoolingDescription','HeatType','ApplianceDesc','YearBuilt','Style','Stories','WaterSource','RoofType','SewerDesc','ParkingDesc','ApproximateLotSize','RoadSurface','WaterfrontFootage','ElementarySchool','MiddleSchool','HighSchool','NeighborhoodAmenities','MonthlyAssocFee','AnnualAssocFee','AssocFeeDesc','AssocFeeIncludes','AnnualMasterAssocFee','AnnualMasterAssocFeeDesc','Taxes','TaxYear','SpecialCircumstances','Status','SearchArea','ListOfficeName','LastChangeTimestamp');

			}
		}
		
		protected function set_detail_result_fields($fields = false){
			if(is_array($fields)){
				
				$this->detail_result_fields = array();
				
				foreach($fields as $f){
					if($this->validate_field($f)){
						$this->detail_result_fields[] = $f;
					}
				}
				
			}else{
				$this->detail_result_fields = array('BathsFull','BathsHalf','City','LocationDesc','PostalCode','CurrentPrice','PropertyType','MLSNumber','PropertySubType','Matrix_Unique_ID','PhotoCount','UnitLocationDesc','BedroomDescription','Amenities','ConstructionDesc','Exterior','FloorDescription','LoadingDesc','LotDesc','RoomsDescription','UtilitiesAvailable','BedsTotal','PublicRemarksConsumerView','StreetNumber','StreetDirPrefix','StreetName','StreetSuffix','StreetDirSuffix','UnitNumber','CountyOrParish','BedsTotal','SubdComplex','Directions','SqFtTotal','SqFtSource','Interior','MasterBathFeatures','BathroomDescription','KitchenFeatures','DiningRoomDescription','LaundryFeaturesLocation','BasementDesc','DisabilityAccess','FireplacesNum','CoolingDescription','HeatType','ApplianceDesc','YearBuilt','Style','Stories','WaterSource','RoofType','SewerDesc','ParkingDesc','ApproximateLotSize','RoadSurface','WaterfrontFootage','ElementarySchool','MiddleSchool','HighSchool','NeighborhoodAmenities','MonthlyAssocFee','AnnualAssocFee','AssocFeeDesc','AssocFeeIncludes','AnnualMasterAssocFee','AnnualMasterAssocFeeDesc','Taxes','TaxYear','SpecialCircumstances','Status','SearchArea','ListOfficeName','UnitLevels','LastChangeTimestamp');
			}
		}
		
		function get_search_result_fields(){
			return $this->search_result_fields;
		}
		
		function get_detail_result_fields(){
			return $this->detail_result_fields;
		}
		
	}
}	
