<?php
if ( !class_exists('Aspk_Rets_Wp_Controller')){
	class Aspk_Rets_Wp_Controller extends Aspk_Rets_Controller{
		protected $endpoint;
		protected $userid;
		protected $password;
		protected $rows_found;
		protected $error;
		protected $perpage;
		protected $current_page;
		
		function __construct(){
			parent::__construct();
			
			$s = $this->get_settings();
			$this->endpoint = $s['rets_end_point'];
			$this->userid = $s['rets_user_id'];
			$this->password = $s['rets_user_password'];
			
		}
		
		function get_settings(){
			$defatuls = $this->rets_settings_default();
			$s = get_option('_aspk_rets_settings',$defatuls);
			return $s;
		}
		
		function rets_settings_default(){
			$defatuls = array();
			$defatuls['rets_end_point'] = '';
			$defatuls['rets_user_id'] = '';
			$defatuls['rets_user_password'] = '';
			return $defatuls;
		}
		
		function we_have_settings(){
			return $this->is_ready();
		}
	}
}	
