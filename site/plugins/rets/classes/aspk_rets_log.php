<?php
if ( !class_exists('Aspk_Rets_Log')){
	class Aspk_Rets_Log{
		
		
		function __construct($from_init = false){
			if($from_init){
				register_activation_hook( 'rets/rets.php', array(&$this, 'install') );
			}
		}
		
		function install(){
			global $aspk_db;
			
			$sql = "
				CREATE TABLE IF NOT EXISTS `share_search_results_log` (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `user_id` int(11) NOT NULL,
				  `search_query` int(11) NOT NULL,
				  `recipient_name` varchar(255) NOT NULL,
				  `recipient_email` varchar(255) NOT NULL,
				  `datetime` datetime NOT NULL,
				  `status` int(11) NOT NULL DEFAULT '0',
				  `subject` varchar(255) NOT NULL,
				  `body` varchar(255) NOT NULL,
				  PRIMARY KEY (`id`)
				) ENGINE=InnoDB DEFAULT CHARSET=latin1;
			";
			$aspk_db->query($sql);
		}
		
		function insert_share_property_log($user_id,$mls_id,$recipient_name,$recipient_email,$datetime,$status,$subject,$body){
			global $aspk_db;
			
			$recipient_name = addslashes($recipient_name);
			$recipient_email = addslashes($recipient_email);
			$body = addslashes($body);
			$sql = "Insert into share_search_results_log (user_id, search_query, recipient_name, recipient_email, datetime, status,subject,body) values({$user_id},{$mls_id},'{$recipient_name}','{$recipient_email}','{$datetime}',{$status},'{$subject}','{$body}')";
			$aspk_db->query($sql);
		}
		
		function select_share_search_results_log($uid){
			global $aspk_db;
			
			$sql = "SELECT * FROM share_search_results_log where user_id = {$uid}";
			return $aspk_db->get_results($sql);
		}
		
		function select_shared_link($mls_id,$recipient_email){
			global $aspk_db;
			
			$recipient_email = addslashes($recipient_email);
			$sql = "SELECT count(id) as number from share_search_results_log where recipient_email = '{$recipient_email}' AND search_query = {$mls_id}";
			return $aspk_db->get_var($sql);
		}
	}
}	
