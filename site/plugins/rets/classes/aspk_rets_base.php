<?php
if ( !class_exists('Aspk_Rets_Base')){
	class Aspk_Rets_Base{
		protected $end_point;
		protected $userid;
		protected $password;
		protected $allowed_fields;
		protected $search_result_fields;
		protected $detail_result_fields;
		
		function __construct(){
			$this->set_allowed_fields();
			
		}
		
		function validate_field($field){
			if(in_array($field,$this->allowed_fields)){
				return true;
			}
			return false;
		}
		
		private function set_allowed_fields(){
			$this->allowed_fields = array();
		}
		
		function set_login($end_point, $userid, $password){
			$this->end_point = $end_point;
			$this->userid = $userid;
			$this->password = $password;
			
		}
	}
}	
