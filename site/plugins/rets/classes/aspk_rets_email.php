<?php
if ( !class_exists('Aspk_Rets_Email')){
	class Aspk_Rets_Email{
		
		
		function __construct($from_init = false){
			if($from_init){
				register_activation_hook( 'rets/rets.php', array(&$this, 'install') );
			}
		}
		
		function install(){
			global $aspk_db;
			
			$sql = "
				CREATE TABLE IF NOT EXISTS `emails_sent` (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `search_id ­` int(11) NOT NULL,
				  `mfrom` varchar(255) DEFAULT NULL,
				  `mto` varchar(255) DEFAULT NULL,
				  `mcc` varchar(255) DEFAULT NULL,
				  `mbcc` varchar(255) DEFAULT NULL,
				  `subject` varchar(255) DEFAULT NULL,
				  `body` text,
				  `msg_sent` datetime DEFAULT NULL,
				  `status` int(11) NOT NULL DEFAULT '0',
				  PRIMARY KEY (`id`)
				) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
			";
			$aspk_db->query($sql);
		}
		
		function html_type(){
			return 'text/html';
		}
		
		function send_email($from, $to, $sid, $subject, $body, $cc='',$bcc=''){
			global $aspk_db;
			
			add_filter( 'wp_mail_content_type', array(&$this, 'html_type') );
			
			$headers[] = 'From: '.$from;
			if(! empty($cc)) $headers[] = 'Cc: '.$cc;
			if(! empty($bcc)) $headers[] = 'Bcc: '.$bcc;
			
			$hdr = file_get_contents(__DIR__ .'header.html');
			$ftr = file_get_contents(__DIR__ .'footer.html');
			$body = $hdr . $body . $ftr;
			
			$ret = wp_mail( $to, $subject, $body, $headers );
			remove_filter( 'wp_mail_content_type', array(&$this,'set_html_content_type' ));
			if($ret){
				$ret = 1;
			}else{
				$ret = 0;
			}
			
			$html = trim(addslashes(htmlspecialchars(html_entity_decode($body, ENT_QUOTES, 'UTF-8'),ENT_QUOTES, 'UTF-8')));
			
			$sql = "Insert into emails_sent (search_id, mfrom, mto, mcc, mbcc, subject, body, msg_sent, status) values({$sid},{$from},{$to},{$cc},{$bcc},{$subject},{$html},now(),{$ret})";
			$aspk_db->query($sql);
			
		}
		
		function select_list_of_emails(){
			global $aspk_db;
			
			$sql = "SELECT * FROM emails_sent";
			return $aspk_db->get_results($sql);
		}
		
		function send_share_property_email($to,$subject,$body,$from){
			add_filter( 'wp_mail_content_type', array(&$this, 'html_type') );
			wp_mail( $to, $subject, $body, $from );
			remove_filter( 'wp_mail_content_type', array(&$this,'set_html_content_type' ));
		}
		
	}
}	
