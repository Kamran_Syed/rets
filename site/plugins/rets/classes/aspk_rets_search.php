<?php
if ( !class_exists('Aspk_Rets_Search')){
	class Aspk_Rets_Search{
		
		
		function __construct($from_init = false){
			if($from_init){
				register_activation_hook( 'rets/rets.php', array(&$this, 'install') );
			}
		}
		
		function get_user_ids_to_run_scheduled_searches(){
			global $aspk_db;
			
			$sql = "SELECT saved_search.id,saved_search.wp_user_id,saved_search.name,saved_search.description,saved_search.criteria,saved_search.last_run,saved_search.saved_by,scheduled_search.search_id ,scheduled_search.additional_emails FROM `scheduled_search`,`saved_search` WHERE scheduled_search.search_id = saved_search.id";
			return $aspk_db->get_results($sql);
		}
		
		function update_search($sid, $kv ){
			global $aspk_db;
			
			$kv = addslashes(serialize($kv));
			$dt = date('Y-m-d H:i:s');
			
			$sql = "UPDATE saved_search set criteria = '{$kv}',
					last_run = '{$dt}'
					where `id`={$sid}";
			$aspk_db->query($sql);	
		}
		
		function update_scheduled_search_results($dt,$sid){
			global $aspk_db;

			$sql = "UPDATE scheduled_search set last_run = '{$dt}' search_id ={$sid}";
			$aspk_db->query($sql);
		}
		
		function save_to_favorite($uid, $mls_id){
			global $aspk_db;
			
			$favorite_searches = $this->get_favorite_by_user($uid);
			if(!empty($favorite_searches)){
				if(!in_array($mls_id, $favorite_searches)){
					array_push($favorite_searches,$mls_id);
				}
			}else{
				$favorite_searches = array($mls_id);
			}
			$favorite_searches = serialize($favorite_searches);
			$sql = "UPDATE idx_user set favorite_properties = '{$favorite_searches}' where `wp_user_id`={$uid}";
			$aspk_db->query($sql);	
		}
		
		function after_remove_favourite_save($uid, $favorite_searches){
            global $aspk_db;
            
            $sql = "UPDATE idx_user set favorite_properties = '{$favorite_searches}' where `wp_user_id`={$uid}";
            $aspk_db->query($sql);    
        }

		
		function after_favourite_remove_hiden_section($uid, $hide_searches){
			global $aspk_db;
			
			$sql = "UPDATE idx_user set hidden_properties = '{$hide_searches}' where `wp_user_id`={$uid}";
			$aspk_db->query($sql);	
		}
		
		function hide_property($uid, $mls_id){
			global $aspk_db;
			
			$hide_searches = $this->get_hidden_by_user($uid);
			if(!empty($hide_searches)){
				if(!in_array($mls_id, $hide_searches)){
					array_push($hide_searches,$mls_id);
				}
			}else{
				$hide_searches = array($mls_id);
			}
			$hide_searches = serialize($hide_searches);
			$sql = "UPDATE idx_user set hidden_properties = '{$hide_searches}' where `wp_user_id`={$uid}";
			$aspk_db->query($sql);	
		}
		
		function check_if_property_is_hidden($uid,$muid){
            global $aspk_db;
			
            $sql = "select hidden_properties from idx_user where wp_user_id = {$uid}";
            $hide_searches = $aspk_db->get_var($sql);
            if(!empty($hide_searches)) {
                $hidden_properties =  unserialize($hide_searches);
                if(in_array($muid,$hidden_properties)){
                    return  true;
                }
            }
            return false;
        }
		
		function is_hidden($uid, $mls_id){
			$hide_searches = $this->get_hidden_by_user($uid);
			if(!empty($hide_searches)){
				if(in_array($mls_id, $hide_searches)){
					return true;
				}
			}
			return false;
		}
		
		function save_search($uid, $kv , $search_name , $cbarray){
			global $aspk_db;
			
			//make sure that name is unique for this user
			//call make_search_name_unique here
				$search_name = $this->make_search_name_unique($uid,$search_name);
				$kv = addslashes(serialize($kv));
				$dt = date('Y-m-d H:i:s');
				$sql = "INSERT INTO saved_search (criteria,wp_user_id,name,last_run,saved_by)  VALUES('{$kv}',{$uid},'{$search_name}','{$dt}','{$cbarray}')";
				$aspk_db->query($sql);	
				return $aspk_db->insert_id;;	
		}
		
		function make_search_name_unique($uid,$name){
			//check if this name already exists for this uid.  if exists, append a digit e.g 1 with name and try again. Repeat this till we get a unique name for this uid. return unique name e.g test7.  If name is already unique just return it.
			global $aspk_db;
			
			$sql = "select name from saved_search where wp_user_id = {$uid} AND name = '{$name}'";
			$result = $aspk_db->get_var($sql);
			if(!$result) return $name;
			$n = 1;
			do{
				$pre_name = explode('-',$result);
				$unique_name = $pre_name[0].'-'.$n;
				$sql = "select name from saved_search where wp_user_id = {$uid} AND name = '{$unique_name}'";
				$new_name = $aspk_db->get_var($sql);
				$n++;
			}while($new_name !='');
			return $unique_name;
		}
		
		 function chk_exis_search($uid , $kv){
			$previous_searches = $this->get_searches_by_user($uid);
			if(!empty($previous_searches)){
				foreach($previous_searches as $ps){
					if(unserialize($ps->criteria) == $kv){
						return $ps->name;
					}
				}
				return false;
			}
		}
		
		function chk_exist_serach_name($uid , $name){
			global $aspk_db;
			
			$sql = "select name from saved_search where wp_user_id = {$uid} AND name = '{$name}'";
			return $aspk_db->get_var($sql);

		}
		
		function chk_exis_name_search($uid , $kv ,$search_name){
			$previous_searches = $this->get_searches_by_user($uid);
			if(!empty($previous_searches)){
				foreach($previous_searches as $ps){
					if(unserialize(stripslashes($ps->criteria)) == $kv){
						return $ps->id;
					}
				}
				return false;
			}
		}
		
		function search_delete($id){
			global $aspk_db;
			
			$sql = "delete from saved_search where id = {$id}";
			return $aspk_db->query($sql);	
		}
		
		function get_searches_by_type($uid,$search_type){
			global $aspk_db;
			
			$sql = "select * from saved_search where wp_user_id = {$uid} AND saved_by = '{$search_type}'";
			return $aspk_db->get_results($sql);	
		}
		
		function get_my_save_search($uid){
			global $aspk_db;
			
			$sql = "select id,name from saved_search where wp_user_id = {$uid} AND saved_by = {$uid}";
			return $aspk_db->get_results($sql);
		}
		
		function get_saved_search_of_user($uid){
			global $aspk_db;
			
			$sql = "select id,name from saved_search where wp_user_id = {$uid}";
			return $aspk_db->get_results($sql);
		}
		function get_shared_searches($uid){
			global $aspk_db;
			
			$sql = "select id,name from saved_search where wp_user_id = {$uid} AND saved_by <> {$uid}";
			return $aspk_db->get_results($sql);
		}
		
		function get_searches_by_user($uid){
			global $aspk_db;
			
			$sql = "select * from saved_search where wp_user_id = {$uid}";
			return $aspk_db->get_results($sql);
		}
		
		function get_search_by_user($uid, $sid){
			global $aspk_db;
			
			$sql = "select * from saved_search where wp_user_id = {$uid} AND id = {$sid}";
			$searches_by = $aspk_db->get_row($sql);
			if(empty($searches_by)) return false;
			$searches_by = unserialize(stripslashes($searches_by->criteria));
			return $searches_by;
		}
		
		function get_search_by_id( $sid){
			global $aspk_db;
			
			$sql = "select criteria from saved_search where id = {$sid}";
			return unserialize(stripslashes($aspk_db->get_var($sql)));
		}
		
		function get_favorite_by_user($uid){
			global $aspk_db;
			
			$sql = "select favorite_properties from idx_user where wp_user_id = {$uid}";
			$favorite_searches = $aspk_db->get_var($sql);
			if(!empty($favorite_searches)) {
				return unserialize($favorite_searches);
			}
			return false;
		}
		
		function get_hidden_by_user($uid){
			global $aspk_db;
			
			$sql = "select hidden_properties from idx_user where wp_user_id = {$uid}";
			$hide_searches = $aspk_db->get_var($sql);
			if(!empty($hide_searches)) {
				return unserialize($hide_searches);
			}
			return false;
		}
		
		function set_last_run_saved($uid, $sid, $dt){
			global $aspk_db;
			
			$sql = "UPDATE saved_search set last_run = '{$dt}' where `id`={$sid}";
			$aspk_db->query($sql);	
		}
		
		function set_last_result($uid, $sid, $st){ //st is 0 or 1 (pass)
			global $aspk_db;
			
			$sql = "UPDATE scheduled_search set last_result = {$st} where `search_id`={$sid}";
			$aspk_db->query($sql);	
		}
		
		function chk_exist_schedule_search($sid){
			global $aspk_db;
			
			$sql = "select id from scheduled_search where search_id = {$sid}";
			return $aspk_db->get_var($sql);
		}
		
		function active_schedule_search($sid){
			global $aspk_db;
			
			$sql = "INSERT INTO scheduled_search (search_id)  VALUES('{$sid}')";
			$aspk_db->query($sql);
		}
		
		function set_last_run_scheduled($uid, $sid, $dt){
			global $aspk_db;
			
			$sql = "UPDATE saved_search set last_run = '{$dt}' where `search_id`={$sid}";
			$aspk_db->query($sql);	
		}
		
		function get_max_saved_searches($uid){
			global $aspk_db;
			
			$sql = "select max_saved_searches from idx_user where wp_user_id = {$uid}";
			$max_saved_searches = $aspk_db->get_var($sql);
			if(empty($max_saved_searches)) return false;
			return $max_saved_searches;
		}
		
		function get_max_scheduled_searches($uid){
			global $aspk_db;
			
			$sql = "select max_scheduled_searches from idx_user where wp_user_id = {$uid}";
			$max_scheduled_searches = $aspk_db->get_var($sql);
			if(empty($max_scheduled_searches)) return false;
			return $max_scheduled_searches;
		}
		
		function get_search_name($sid){
			global $aspk_db;
			
			$sql = "select name from saved_search where id = {$sid}";
			return $aspk_db->get_var($sql);
			
		}
		
		function get_schedule_search_result($wp_uid){
			global $aspk_db;
			
			$sql = "SELECT scheduled_search.search_id,scheduled_search.id,scheduled_search.additional_emails,saved_search.name FROM `scheduled_search`,`saved_search` Where scheduled_search.search_id = saved_search.id AND saved_search.wp_user_id = '{$wp_uid}'";
			return $aspk_db->get_results($sql);
		}
		
		function remove_search_notification($sch_id){
			global $aspk_db;
			
			$sql = "delete from scheduled_search where id = {$sch_id}";
			return $aspk_db->query($sql);
		}
		
		function install(){
			global $aspk_db;
			
			$sql = "
				CREATE TABLE IF NOT EXISTS  `idx_user` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`wp_user_id` decimal(10,0) NOT NULL,
				`children` text,
				`max_saved_searches` int(11) NOT NULL,
				`max_scheduled_searches` int(11) NOT NULL,
				`saved_properties` text,
				`hidden_properties` text,
				`favorite_properties` text,
				`home_street` varchar(100) DEFAULT NULL,
				`home_city` varchar(50) DEFAULT NULL,
				`home_state` varchar(50) DEFAULT NULL,
				`home_zip` varchar(50) DEFAULT NULL,
				`work_street` varchar(100) DEFAULT NULL,
				`work_city` varchar(50) DEFAULT NULL,
				`work_state` varchar(50) DEFAULT NULL,
				`work_zip` varchar(50) DEFAULT NULL,
				`phone` varchar(255) DEFAULT NULL,
				`name` varchar(255) DEFAULT NULL,
				`email` varchar(255) DEFAULT NULL,
				PRIMARY KEY (`id`)
				) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
			";
			$aspk_db->query($sql);
			
			$sql = "
				CREATE TABLE IF NOT EXISTS `saved_search` (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `wp_user_id` decimal(10,0) NOT NULL,
				  `name` varchar(60) DEFAULT NULL,
				  `description` varchar(255) DEFAULT NULL,
				  `criteria` text,
				  `last_run` datetime DEFAULT NULL,
				  `saved_by` varchar(255) NOT NULL,
				  PRIMARY KEY (`id`)
				) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
			";
			$aspk_db->query($sql);
			
			$sql = "
				CREATE TABLE `scheduled_search` (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `search_id` int(11) NOT NULL,
				  `last_run` datetime DEFAULT NULL,
				  `last_result` int(11) NOT NULL DEFAULT '0',
				  `additional_emails` text,
				  PRIMARY KEY (`id`)
				) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
			";
			$aspk_db->query($sql);
			
		}
		
		function show_children_checkbox_html($uid){
			$rls = new Aspk_Rets_Roles();
			$chd = $rls->get_children($uid);
			if($chd){
				foreach($chd as $c){
					$data = get_userdata($c);
					?>
						<input type="checkbox" name="<?php echo $data->user_login;?>" value="<?php echo $data->ID;?>"><?php echo $data->user_login;?><br>
					<?php
				}
			}
		}
		
		function get_user_id_by_email($email){
			$user = get_user_by( 'email', $email);
			if($user){
				return $user->data->ID;
			}
			
		}
		
		function show_save_search_form($uid){
			$ask_detail = false;

			$rl = new Aspk_Rets_Roles();
			if($rl->has_role($uid, 'agent')) $ask_detail = true;
			if($rl->has_role($uid, 'staff')) $ask_detail = true;
			if($rl->has_role($uid, 'broker')) $ask_detail = true;
			?>
				<div style = "float:left;clear:left;">
					<div id="save_msg" style="display:none;">Search has been saved</div>
					<div>
					<?php if(isset($_POST['edit_search'])){
						?><input type = "hidden" id = "x_srch_id" value = "<?php echo $_POST['edit_search']; ?>" /><?php
					} ?>
						<label>Search Name <input id="search_name"type="text" name="search_name"<?php if(isset($_POST['edit_search'])) echo "readonly"; ?> value="<?php if(isset($_POST['edit_search'])) echo $this->get_search_name($_POST['edit_search']);?>"/></label><?php 
						if(!$uid){ ?>
							<input type="button" name="save_search" value="Save Search" onclick="tb_show('Sign up', url);return false;"/><?php
						}else{ ?>
							<input type="button" name="save_search" value="Save Search" onclick="return save_search()"/><?php
						} ?>
					</div>
					<?php if($ask_detail){ ?>
						<div>Save Search To</div>
						<div id="has_more_info">
							<input type="checkbox" name="me" value="<?php echo $uid; ?>" checked>Me<br>
							<input type="checkbox" name="t24" value="<?php echo $this->get_user_id_by_email('terrace@gmail.com'); ?>">Terrace 24<br>
							<input type="checkbox" name="public" value="<?php echo $this->get_user_id_by_email('public@gmail.com'); ?>">Public<br>
							<?php $this->show_children_checkbox_html($uid); ?>
						</div>
					<?php }/* else{
						?>
						<div id="has_more_info">
							<input type="checkbox" name="me" value="me" checked>Me<br>
						</div>
						<?php
					} */ //ends detail?>
				</div><!--wrap-->
			<?php
		}
		
		function notify_me(){
				if(isset($_GET['srch_id'])){
					$sid = $_GET['srch_id'];
				}else{
					$sid = '';
				}
			?>
			<div style = "float:left;clear:left;">
				<input type="hidden" id = "search_id" value="<?php echo $sid;?>"/>
				<div style = "float:left;width:26em;">Notify me when there are new properties against this search</div>
				<div style = "float:left;width:10em;"><input onclick="notify_search(this)" type = "button" id = "notify_me" value = "Notify" /></div>
			</div>
			<?php
		}
		
	}
}		