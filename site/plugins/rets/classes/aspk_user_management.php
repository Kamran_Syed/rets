<?php
if ( !class_exists('Aspk_User_Management')){
	class Aspk_User_Management{
		function get_user_info($uid){
			global $aspk_db;
			
			$sql = "select * from idx_user where wp_user_id = {$uid}";
			$searches_by = $aspk_db->get_row($sql);
			if(empty($searches_by)) return false;
			return $searches_by;
		}
		
		function isert_wp_user_idx($uid){
			global $aspk_db;
			
			$sql = "INSERT INTO idx_user (wp_user_id) VALUES({$uid})";
			$aspk_db->query($sql);	
		}
		
		function update_info($uid,$name,$email,$phone,$home_st,$home_city,$home_state,$home_zip,$work_st,$work_city,$work_state,$work_zip){
			global $aspk_db;
			
			$sql = "UPDATE idx_user set
					home_street = '{$home_st}' ,
					home_city = '{$home_city}' ,
					home_state = '{$home_state}' ,
					home_zip = '{$home_zip}' ,
					work_street = '{$work_st}' ,
					work_city = '{$work_city}' ,
					work_state = '{$work_state}' ,
					work_zip = '{$work_zip}' ,
					phone = '{$phone}' ,
					name = '{$name}' ,
					email = '{$email}' 
					where `wp_user_id`={$uid}";
			$aspk_db->query($sql);	
		}
		function wp_email_update($uid,$email){
			global $wpdb;
			$sql = "UPDATE {$wpdb->prefix}users set
					user_email = '{$email}'  
					where `ID`={$uid}";
			$wpdb->query($sql);	
		}
	}
}	
