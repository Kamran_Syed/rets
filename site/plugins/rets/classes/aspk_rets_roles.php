<?php
if ( !class_exists('Aspk_Rets_Roles')){
	class Aspk_Rets_Roles{
		
		
		function __construct($from_init = false){
			if($from_init){
				register_activation_hook( 'rets/rets.php', array(&$this, 'install') );
				register_deactivation_hook( 'rets/rets.php', array(&$this, 'remove') );
			}
		}
		
		function install(){
			$sub = get_role( 'Subscriber' );
			$caps = $sub->capabilities;
			add_role('client',  __( 'Client' ), $caps);
			add_role('prospect',  __( 'Prospect' ), $caps);
			add_role('agent',  __( 'Agent' ), $caps);
			add_role('staff',  __( 'Staff' ), $caps);
			add_role('broker',  __( 'Broker' ), $caps);
			
			
		}
		
		function remove(){
			remove_role( 'client' );
			remove_role( 'prospect' );
			remove_role( 'agent' );
			remove_role( 'staff' );
			remove_role( 'broker' );
		}
		
		private function remove_deleted_users($users){
			//array of uid
			$updated_users = array();
			if(!$users) {
				return $updated_users;
			}
			foreach($users as $uid){
				$user = get_userdata( $uid );
				if ( $user === false ) {
					continue;
				}
				$updated_users[] = $uid;
			}
			return $updated_users;
		}
		
		function add_child($uid, $child_uid){
			$parent = get_userdata( $uid );
			if ( $user === false ) {
				return false;
			}
			
			$child = get_userdata( $child_uid );
			if ( $user === false ) {
				return false;
			}
			
			$children = get_user_meta( $uid, 'child_accounts', true );
			$children = $this->remove_deleted_users($children);
			
			if(! in_array($child_uid, $children)){
				$children[] = $child_uid;
				update_user_meta($uid, 'child_accounts', $children);
			}
			
		}
		
		function remove_child($uid, $child_uid){
			$parent = get_userdata( $uid );
			if ( $user === false ) {
				return false;
			}
			
			$children = get_user_meta( $uid, 'child_accounts', true );
			$children = $this->remove_deleted_users($children);
			
			if(in_array($child_uid, $children)){
				$new_family = array();
				
				foreach($children as $cuid){
					if($cuid == $child_uid) continue;
					$new_family[] = $cuid;
				}
				update_user_meta($uid, 'child_accounts', $new_family);
			}
			
		}
		
		function get_children($uid){
			$children = get_user_meta( $uid, 'child_accounts', true );
			return $children;
		}
		
		function has_role($uid, $role){
			//has to be role slug
			$user = new WP_User( $uid );

			if ( !empty( $user->roles ) && is_array( $user->roles ) ) {
				foreach ( $user->roles as $user_role )
					if($role == $user_role ) return true;
			}
			return false;
		}
		
	}
}	
