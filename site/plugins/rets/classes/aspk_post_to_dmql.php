<?php
if ( !class_exists('Aspk_Post_To_Dmql')){
	class Aspk_Post_To_Dmql{
		private $suffix_list;
		private $fmls;
		private $limit;
		private $sql;
		private $specialcircumstances;
		private $pairs;
		private $sskv;
		
		function __construct(){
			$this->fields = array();
			$this->suffix_list = array();
			$this->set_suffix_list();
			$this->fmls = new Aspk_Rets_Fmls();
			$this->limit = 20; //default
			$this->specialcircumstances = "";
		}
		
		function get_dmql(&$http_post){
			$fields = $this->clean_post($http_post);
			$sql = $this->build_sql($fields);
			$dmql = $this->build_dmql($sql);
			return $dmql;
		}
		
		function set_limit($limit){
			$this->limit = $limit;
		}
		
		function get_sql(){
			return $this->sql;
		}
		
		private function build_sql(&$fields){  //kv pair
			
			$this->pairs = array();
			
			foreach($fields as $k=>$v){
				if(empty($v)) continue;
				
				$v = $this->rets_encode($k,$v);
				
				$nv = $this->build_nv_pair($k,$v);
				$this->pairs[] = $nv;
			}
			
			$sql = implode(',', $this->pairs);
			$this->sql = $sql;
			
			return $sql;
		}
		
		private function rets_encode($k,$v){
			
			$k = $this->get_field($k);
			$suffix = $this->get_suffix($k);
			$k = strtolower($k);
			
			switch($k){
				case 'city':
					$v = strtoupper(substr($v, 0, 6));
					break;
				case 'status':
					if($v = 'active'){
						$v = 'A';
					}
					break;
				case 'stories':
					$v = 'ONEST)|(UnitLevels=1LEVL';
					break;
				case 'specialcircumstances':
				
					if(!empty($this->specialcircumstances)){
						$tmp = str_replace('POTSS,SSPAP','POTSS',$this->specialcircumstances);
						$tmp = str_replace('+','',$tmp);
						$tmp = str_replace('|','',$tmp);
						if(strstr('SSPAP,POTSS',$v)){
							$tmp = str_replace('POTSS,','',$tmp);
							$v = '+'.$tmp;
						}else{
							$v = '+'.$tmp.','.$v;
						} 
						
					}
					$this->specialcircumstances = $v;
					
					break;
					
			}
			
			return $v;
		}
		
		function get_page_listing_dmql($sql, $start){
			
			if($start > $this->limit){
				$this->limit = $this->limit + $start;
			}
			
			$dmql = $this->build_page_dmql($sql);
			
			return $dmql;
		}
		
		function get_page_sql($results){
			$sql = implode(',', $results);
			$sql = '(Matrix_Unique_ID='.$sql.')';
			
			return $sql;
		}
		
		private function build_nv_pair($k,$v){
			$field = $this->get_field($k);
			$suffix = $this->get_suffix($k);
			
			$nv ="(";
			
			if($field == 'SpecialCircumstances' && strstr($v, '+') ){
				//This is special treatment 
				
				if(($key = array_search($this->sskv, $this->pairs)) !== false) {
					unset($this->pairs[$key]);
					$this->pairs = array_values($this->pairs);
				}
				
				$suffix = false;
				
			}
			
			switch($suffix){
				case false:
					$nv .= "{$field}={$v}";
					break;
				case 'from':
					$nv .= "{$field}={$v}+";
					break;
				case 'upto':
					$nv .= "{$field}={$v}-";
					break;
				case 'like':
					$nv .= "{$field}=*{$v}*";
					break;
				case 'any':
					$nv .= "{$field}=|{$v}";
					break;
				case 'not':
					$nv .= "{$field}=~{$v}";
					break;
				case 'all':
					$nv .= "{$field}=+{$v}";
					break;
			}
			
			$nv .= ")";
			
			if($field == 'SpecialCircumstances') $this->sskv = $nv;
			
			return $nv;
		}
		
		private function build_dmql($sql){
			
			$dmql = array();
			
			$select = implode(',',$this->fmls->get_search_result_fields());
			
			$para = array(
				'Format'	=> 'COMPACT-DECODED',
				'Select'	=> $select,
				'Count'		=> 1,
				'Limit'		=> $this->limit
			);
			
			$dmql = array('resource'=>'Property', 'class'=>'Listing', 'sql'=>$sql, 'para'=>$para);
			
			return $dmql;
		}
		
		private function build_page_dmql($sql){
			
			$dmql = array();
			
			$para = array(
				'Format'	=> 'COMPACT-DECODED',
				'Select'	=> 'Matrix_Unique_ID',
				'Count'		=> 1,
				'Limit'		=> $this->limit
			);
			
			$dmql = array('resource'=>'Property', 'class'=>'Listing', 'sql'=>$sql, 'para'=>$para);
			
			return $dmql;
		}
		
		function get_detail_dmql($muid){
			
			$dmql = array();
			
			$select = implode(',',$this->fmls->get_detail_result_fields());
			
			$sql = '(Matrix_Unique_ID='.$muid.')';
			
			$para = array(
				'Format'	=> 'COMPACT-DECODED',
				'Select'	=> $select,
				'Count'		=> 1,
				'Limit'		=> 1
			);
			
			$dmql = array('resource'=>'Property', 'class'=>'Listing', 'sql'=>$sql, 'para'=>$para);
			
			return $dmql;
		}
		
		function mls_to_matrix($mls){
			
			$dmql = array();
			
			$sql = '(MLSNumber='.$mls.')';
			
			$para = array(
				'Format'	=> 'COMPACT-DECODED',
				'Select'	=> 'Matrix_Unique_ID',
				'Count'		=> 1,
				'Limit'		=> 1
			);
			
			$dmql = array('resource'=>'Property', 'class'=>'Listing', 'sql'=>$sql, 'para'=>$para);
			
			return $dmql;
		}
		
		private function clean_post(&$http_post){
			$clean_post = array();
			
			foreach($http_post as $k => $v){
				
				if($this->get_field($k)){
					$clean_post[$k] = $v;
					
				}elseif(strtolower($k) == 'mls_record_limit'){
					if(is_int($v) && intval($v) < 200){  //safety net
						$this->limit = $v;
					}
					return false;
				}
			} 
			
			return $clean_post;
		}
		
		private function get_field($field){
			
			if(! strstr($field, '-')){
				if($this->fmls->validate_field($field)) return $field;
			}
			
			foreach($this->suffix_list as $item){
				$len = strlen($field) - strlen('-'.$item);
				$tf = substr($field, 0, $len);
				
				if($this->fmls->validate_field($tf)) return $tf;
			}
			return false;
		}
		
		private function get_suffix($field){
			$f = $this->get_field($field);
			if(!$f) return false;
			
			if($f == $field) return false; //no suffix
			
			$may_be_suffix = str_ireplace($f.'-','',$field);
			
			$may_be_suffix = strtolower($may_be_suffix);
			
			if(in_array($may_be_suffix, $this->suffix_list)){
				return $may_be_suffix;
			}
			
			return false;
		}
		
		private function set_suffix_list(){
			$this->suffix_list = array('from','upto','like','any','all','not');
		}
		
		private function validate_suffix($suffix){
			if(in_array(strtolower($suffix),$this->suffix_list)){
				return true;
			}
			return false;
		}
	}
}	
