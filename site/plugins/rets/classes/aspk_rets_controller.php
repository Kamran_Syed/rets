<?php
if ( !class_exists('Aspk_Rets_Controller')){
	class Aspk_Rets_Controller{
		protected $endpoint;
		protected $userid;
		protected $password;
		protected $rows_found;
		protected $error;
		protected $perpage;
		protected $current_page;
		
		function __construct(){
			$this->perpage = 10;
			$this->current_page = 1;
		}
		
		protected function is_ready(){
			if(!$this->endpoint || !$this->userid || !$this->password){
				return false;
			}
			return true;
		}
		
		function get_current_page(){
			return $this->current_page;
		}
		
		function get_results($post, $page=1){
			$this->rows_found = 0;
			$this->error = NULL;
			
			$rets = new phRETS;

			$connect = $rets->Connect($this->endpoint, $this->userid , $this->password);
			
			if($connect) {
				$ptd = new Aspk_Post_To_Dmql();
				$ptd->set_limit($this->perpage);
				
				if(intval($page) < 1) $page = 1;
				if(empty($_SESSION['aspk_dmql'])) $page = 1;
				
				if($page == 1){
					$dmql = $ptd->get_dmql($post);
					$_SESSION['aspk_dmql'] = $dmql;
				}else{
					$this->current_page = $page;
					$orig_dmql = $_SESSION['aspk_dmql'];
					
					$sql = $orig_dmql['sql'];
					
					$start = ($page -1) * $this->perpage + 1;
					$dmql = $ptd->get_page_listing_dmql($sql, $start);
					
					$search = $rets->SearchQuery($dmql['resource'],$dmql['class'],$dmql['sql'],$dmql['para']);
					$this->rows_found = $rets->TotalRecordsFound();
					
					$results = array();
					if($rets->TotalRecordsFound() > 0) {
						while($data = $rets->FetchRow($search)) {
							$results[] = $data['Matrix_Unique_ID'];
						}
						
						array_splice($results, 0, count($results) - $this->perpage);
						
						$sql = implode(',', $results);
						$sql = '(Matrix_Unique_ID='.$sql.')';
						
						$orig_dmql['sql'] = $sql;
						$dmql = $orig_dmql;
					}
					
				}
				
				$search = $rets->SearchQuery($dmql['resource'],$dmql['class'],$dmql['sql'],$dmql['para']);
				
				if($this->rows_found == 0) $this->rows_found = $rets->TotalRecordsFound();

				if($page ==1){
					$logger = Aspk_Rets_Logger::getInstance();
					$logger->log_post($post, $this->rows_found);
				}
				
				$results = array();
				if($rets->TotalRecordsFound() > 0) {
					while($data = $rets->FetchRow($search)) {
						$results[] = $data;
					}
				}
				
				$rets->FreeResult($search);
				$rets->Disconnect();
				
				return json_encode($results);
				
			}else{
				$this->error = $rets->Error();
				return false;
			}
			
		}
		
		function get_all_results($post, $page=1){
			$this->rows_found = 0;
			$this->error = NULL;
			
			$rets = new phRETS;

			$connect = $rets->Connect($this->endpoint, $this->userid , $this->password);
			
			if($connect) {
				$ptd = new Aspk_Post_To_Dmql();
				$ptd->set_limit(1000);
				
				$page = 1;
				
				if($page == 1){
					$dmql = $ptd->get_dmql($post);
				}
				
				$search = $rets->SearchQuery($dmql['resource'],$dmql['class'],$dmql['sql'],$dmql['para']);
				
				$results = array();
				if($rets->TotalRecordsFound() > 0) {
					while($data = $rets->FetchRow($search)) {
						$results[] = $data;
					}
				}
				
				$rets->FreeResult($search);
				$rets->Disconnect();
				
				return json_encode($results);
				
			}else{
				$this->error = $rets->Error();
				return false;
			}
			
		}
		
		function get_property($muid){
			$this->rows_found = 0;
			$this->error = NULL;
			
			$rets = new phRETS;

			$connect = $rets->Connect($this->endpoint, $this->userid , $this->password);
			
			if($connect) {
				$ptd = new Aspk_Post_To_Dmql();
				$ptd->set_limit(1);
				
				$dmql = $ptd->get_detail_dmql($muid);

				$search = $rets->SearchQuery($dmql['resource'],$dmql['class'],$dmql['sql'],$dmql['para']);
				$this->rows_found = $rets->TotalRecordsFound();
				
				$data = '';
				if($rets->TotalRecordsFound() > 0) {
					while($data = $rets->FetchRow($search)) {
						
						break;
					}
				}
				
				$rets->FreeResult($search);
				$rets->Disconnect();
				
				return json_encode($data);
				
			}else{
				$this->error = $rets->Error();
				return false;
			}
			
		}
		
		function mls_to_matrix($mls){
			$this->rows_found = 0;
			$this->error = NULL;
			
			$rets = new phRETS;

			$connect = $rets->Connect($this->endpoint, $this->userid , $this->password);
			
			if($connect) {
				$ptd = new Aspk_Post_To_Dmql();
				$ptd->set_limit(1);
				
				$dmql = $ptd->mls_to_matrix($mls);

				$search = $rets->SearchQuery($dmql['resource'],$dmql['class'],$dmql['sql'],$dmql['para']);
				$this->rows_found = $rets->TotalRecordsFound();
				
				if($rets->TotalRecordsFound() > 0) {
					while($data = $rets->FetchRow($search)) {
						
						break;
					}
				}
				
				$rets->FreeResult($search);
				$rets->Disconnect();
				
				return $data['Matrix_Unique_ID'];
				
			}else{
				$this->error = $rets->Error();
				return false;
			}
			
		}
		
		function get_last_error(){
			return $this->error;
		}
		
		function records_found(){
			return $this->rows_found;
		}
		
		function get_total_pages(){
			if($this->rows_found > 0 && $this->perpage > 1){
				if($this->rows_found < $this->perpage){
					return 1;
				}
				return floor($this->rows_found / $this->perpage);
			}
			return 0;
		}
		
		function get_results_per_page(){
			return $this->perpage;
		}
		
		function set_results_per_page($perpage){
			$perpage = intval($perpage);
			
			if($perpage > 1){
				$this->perpage = $perpage;
				return true;
			}
			
			return false;
		}
		
		function get_images($mui){
			$this->rows_found = 0;
			$this->error = NULL;
			
			$rets = new phRETS;
			$connect = $rets->Connect($this->endpoint, $this->userid , $this->password);
			
			if($connect) {
				$photos = $rets->GetObject('Property', 'LargePhoto', intval($mui));//matrix unique id is to be used and not mls id
				$rets->Disconnect();
				
				return $photos;
				
			}else{
				$this->error = $rets->Error();
				return false;
			}
		}
		
		function get_image_small($mui){
			$this->rows_found = 0;
			$this->error = NULL;
			
			$rets = new phRETS;
			$connect = $rets->Connect($this->endpoint, $this->userid , $this->password);
			
			if($connect) {
				$photos = $rets->GetObject('Property', 'Photo', intval($mui));//matrix unique id is to be used and not mls id
				$rets->Disconnect();
				
				return $photos;
				
			}else{
				$this->error = $rets->Error();
				return false;
			}
		}
	}
}	
