<?php

/**
 * Plugin Name: RETS
 * Plugin URI: 
 * Description: RETS Plugin
 * Version: 3.8
 * Author: Agile Solutions PK
 * Author URI:
 */

//db Settings
$db_user_id = 'mls';
$db_password = 'juytr';
$db_name = 'mls';
$db_host_name = 'ff.com';

//

global $aspk_db;

if(! isset($aspk_db)){
	$aspk_db = new wpdb($db_user_id, $db_password, $db_name, $db_host_name);
}


require_once(__DIR__ ."/classes/phrets.php");
require_once(__DIR__ ."/classes/aspk_rets_logger.php");  
require_once(__DIR__ ."/classes/aspk_post_to_dmql.php");  
require_once(__DIR__ ."/classes/aspk_rets_base.php"); 
require_once(__DIR__ ."/classes/aspk_rets_fmls.php"); 
require_once(__DIR__ ."/classes/aspk_rets_controller.php"); 
require_once(__DIR__ ."/classes/aspk_rets_wp_controller.php"); 
require_once(__DIR__ ."/classes/aspk_rets_roles.php"); 
require_once(__DIR__ ."/classes/aspk_rets_search.php"); 
require_once(__DIR__ ."/classes/aspk_rets_email.php"); 
require_once(__DIR__ ."/classes/aspk_user_management.php"); 
require_once(__DIR__ ."/classes/aspk_rets_log.php"); 



if ( !class_exists('Agile_Rets_Aspk')){
	class Agile_Rets_Aspk{
		function __construct(){
			register_activation_hook( __FILE__, array(&$this, 'install') );
			add_shortcode( 'aspk_rets_search', array(&$this,'basic_rets'));
			add_shortcode( 'aspk_dashboard', array(&$this,'user_dashboard'));
			add_action('admin_menu', array(&$this, 'admin_menu'));
			add_action('after_setup_theme', array(&$this, 'old_init'),1);
			add_action('wp_head',array(&$this, 'wp_head'));
			add_action( 'wp_enqueue_scripts', array(&$this, 'enqueue_scripts' ));
			add_action( 'admin_enqueue_scripts', array(&$this, 'admin_enqueue_scripts' ));
			add_action( 'login_enqueue_scripts', array(&$this, 'login_enqueue_scripts' ));
			add_action( 'wp_login', array(&$this, 'wp_login' ), 10, 2);
			add_action( 'init', array(&$this, 'init' ));
			add_action( 'wp_footer', array(&$this, 'wp_footer' ));
			add_action( 'user_register', array(&$this, 'create_cms_contact' ));
			add_action( 'wp_ajax_aspk_add_favourite', array(&$this,'add_favourite' ));
			add_action( 'wp_ajax_aspk_remove_favourite', array(&$this,'remove_favourite' ));
			add_action( 'wp_ajax_aspk_hide_property', array(&$this,'hide_property' ));
			add_action( 'wp_ajax_aspk_unhide_property', array(&$this,'unhide_property' ));
			add_action( 'wp_ajax_aspk_share_propery_results', array(&$this,'share_propery_results' ));
			add_action( 'wp_ajax_nopriv_aspk_share_propery_results', array(&$this,'share_propery_results' ));
			add_action( 'wp_ajax_nopriv_aspk_auto_send_results', array(&$this,'auto_send_results' ));
			add_action( 'wp_ajax_aspk_save_search', array(&$this,'save_search' ));
			add_action( 'wp_ajax_aspk_search_del', array(&$this,'aspk_search_del' ));
			add_action( 'wp_ajax_aspk_notify_search', array(&$this,'notify_search' ));
			add_action( 'wp_ajax_aspk_schedule_search', array(&$this,'aspk_stop_schedule_search' ));
			add_action( 'wp_ajax_aspk_schedule_search_del', array(&$this,'aspk_schedule_search_del' ));
			add_action( 'wp_ajax_aspk_share_saved_search', array(&$this,'share_saved_search' ));
			
			$logger = Aspk_Rets_Logger::getInstance();
			new Aspk_Rets_Roles(true);
			new Aspk_Rets_Search(true);
			new Aspk_Rets_Email(true);
			new Aspk_Rets_Log(true);
			
		}

		function aspk_schedule_search_del(){
			$s = new Aspk_Rets_Search();
			$s->remove_search_notification($_POST['schedule_id']);
			$s->search_delete($_POST['search_id']);
			$ret = array('st' => 'ok','msg'=>'Search has been Deleted successful');
			echo json_encode($ret);
			exit;
		}
		
		function aspk_stop_schedule_search(){
			$s = new Aspk_Rets_Search();
			$s->remove_search_notification($_POST['schedule_id']);
			$ret = array('st' => 'ok','msg'=>'Search has been stop successful');
			echo json_encode($ret);
			exit;
		}
		function share_saved_search(){
			$email_obj = new Aspk_Rets_Email();
			$log = new Aspk_Rets_Log();
			
			$search_ids = $_POST['search_id'];
			$permalink =  $_POST['permalink'];
			$recipient_email = $_POST['your_email'];
			$recipient_name = $_POST['your_name'];
			$search_id = explode(',',$search_ids);
			$search_id = $search_id[0];
			$user_id = get_current_user_id();
			$user_info = get_userdata($user_id);
			$status = 2;
			$datetime = date('Y-m-d H:i:s');
			$subject = "Shared Search";
			$message = "Dear $recipient_name a property search has been shared with you for visit please click on this link  <a href=".$_POST['permalink'].'?srch_id='.$search_id.">View Shared Search</a>";
			
			if($user_info->display_name){
				$name = $user_info->display_name;
			}elseif($user_info->user_nicename){
				$name = $user_info->user_nicename;
			}else{
				$name = $user_info->user_login;
			}
			$from = "From: $name <".$user_info->user_email.">\r\n";
			
			if (!filter_var($recipient_email, FILTER_VALIDATE_EMAIL)) {
			  $ret = array('st' => 'err','msg'=>'Invalid email format');
			  echo json_encode($ret);
			  exit;
			}
			
			if($search_id && $recipient_email && $recipient_name){
				$count = $log->select_shared_link($search_id,$recipient_email);
				if($count >0){
					$ret = array('st' => 'err','msg'=>'This Search has already been shared to this email');
					echo json_encode($ret);
				}else{
					$log->insert_share_property_log($user_id,$search_id,$recipient_name,$recipient_email,$datetime,$status,$subject,$message);
					$email_obj->send_share_property_email($recipient_email,$subject,$message,$from);
					$ret = array('st' => 'ok','msg'=>'Search has been shared successfully');
					echo json_encode($ret);
				}
			}
			exit;
		}
		
		function notify_search(){
			$s = new Aspk_Rets_Search();
			if($_POST['edit_search_id']){
				$sid = $_POST['edit_search_id'];
				$exist_id = $s->chk_exist_schedule_search($sid);
				if($exist_id){
					$ret = array('st' => 'er','msg'=>'Search schedule already exist');
					echo json_encode($ret);
					exit;
				}else{
					$s->active_schedule_search($sid);
					$ret = array('st' => 'ok','msg'=>'Your search has been notified');
					echo json_encode($ret);
					exit;
				}
			}else{
				$search_id_string = $_POST['search_id_string'];
				if($search_id_string){
					$search_id_string = explode(',',$_POST['search_id_string']);
					foreach($search_id_string as $sid){
						$exist_id = $s->chk_exist_schedule_search($sid);
						if($exist_id){
							$ret = array('st' => 'er','msg'=>'Search schedule already exist');
							echo json_encode($ret);
							exit;
						}else{
							$s->active_schedule_search($sid);
						}
					}
					$ret = array('st' => 'ok','msg'=>'Your search has been notified');
					echo json_encode($ret);
					exit;
				}else{
					$search_name = $_POST['search_name'];
					if(!$search_name) $search_name = 'annoymous';
					$kv = $_SESSION['aspk_search_fields'];
					$uid = $s->get_user_id_by_email('annoymous@gmail.com');
					$sid = $s->save_search($uid, $kv , $search_name, $uid);
					$s->active_schedule_search($sid);
					$ret = array('st' => 'ok','msg'=>'Your search has been notified');
					echo json_encode($ret);
					exit;
				}
			}			
			
		}
		
		function auto_send_results(){
			$rs_obj =  new Aspk_Rets_Search();
			$ctl = new Aspk_Rets_Wp_Controller();
			$email_obj = new Aspk_Rets_Email();
			$save_searchs = $rs_obj->get_user_ids_to_run_scheduled_searches();
			$mlsdt_fromat =  date('Y-m-d\TH:i:s', time());
			if($save_searchs){
				foreach($save_searchs as $save_searchs){
					$uid = $save_searchs->wp_user_id;
					$sid = $save_searchs->id;
					$user = get_user_by( 'id', $uid );
					$email = $user->user_email;
					$criteria = unserialize(stripslashes($save_searchs->criteria));
					$saved_time = get_user_meta($uid,'__aspk_save_searched_time',true);//get from user meta
					if($saved_time[$sid]){
						$criteria['LastChangeTimestamp-From'] = $saved_time[$sid];
					}
					$raw = $ctl->get_results($criteria, null);
					$saved_time[$sid] = $mlsdt_fromat;
					update_user_meta($uid,'__aspk_save_searched_time',$saved_time);//update user meta with current time
					$results = json_decode($raw);
					$rs_obj->update_scheduled_search_results($mlsdt_fromat,$sid);
					ob_start();
					if($results){
						foreach($results as $res){
							$this->cron_mail_html($uid,$res);
						}
						$html = ob_get_clean();
						$from = "From: Terrace24 <noreply@terrace24.com>\r\n";
						$email_obj->send_share_property_email($email,'New Properties Found for Your Search',$html,$from);
					}
				}
			}
			exit;
		}
		
		function upload_image_binary($binary,$uid,$propid){
			$upload_dir = wp_upload_dir();
			$path = $upload_dir['path'].'/'.$propid.$uid.'.jpg';
			file_put_contents($path, $binary);
			return  $upload_dir['url'].'/'.$propid.$uid.'.jpg';
		}
		
		function cron_mail_html($uid,$r){ 
			$ctl = new Aspk_Rets_Wp_Controller();
			$img = $ctl->get_image_small($r->Matrix_Unique_ID);
			$permalink = $this->get_rets_search_page_link();
			?>
				<div style="clear:left;float:left;margin-bottom:1em;" id="prop_<?php echo $r->MLSNumber; ?>">
					<div style="float:left;">
						<div style="width:15em;">
						<?php if(isset($img[0]['Data'])){
							$url = $this->upload_image_binary($img[0]['Data'],$uid,$r->Matrix_Unique_ID);
							?>
							<img src="<?php echo $url; ?>"/>
						<?php }else{ ?>
							<img src="<?php echo plugins_url( 'img/no_image.png', __FILE__ );?>" alt="No Image Available" width="400">
						<?php } ?>
						</div>
					</div>
					<div style="clear:left;float:left;">
						<a href="<?php echo $permalink.'?propid='.$r->Matrix_Unique_ID.'&mls_id='.$r->MLSNumber;?>"> View Property</a>
					</div>
					<div style="clear:left;float:left;"><span class="t24-det-val t24-mls-StreetNumber"><?php echo $r->StreetNumber.'</span><span class="t24-det-val t24-mls-StreetDirPrefix"> '.$r->StreetDirPrefix.'</span><span class="t24-det-val t24-mls-StreetName"> '.$r->StreetName.'</span><span class="t24-det-val t24-mls-StreetSuffix"> '.$r->StreetSuffix.'</span><span class="t24-det-val t24-mls-StreetDirSuffix"> '.$r->StreetDirSuffix;?></span></div>
					<div style="clear:left;<?php if(empty($r->UnitNumber)) echo 'display:none;';?>"><span class="t24-det-name">Unit #</span><span class="t24-det-val t24-mls-UnitNumber"> <?php echo $r->UnitNumber;?></span></div>
					<div style="clear:left;margin-top:1em;"><span class="t24-det-val t24-mls-City"><?php echo $r->City.'</span><span class="t24-det-val t24-mls-PostalCode"> '.$r->PostalCode.'</span><span class="t24-det-val t24-mls-CountyOrParish"> '.$r->CountyOrParish.'</span> County';?></div>
					<!--<div><span class="t24-det-name">Status:</span><span class="t24-det-val t24-mls-Status"> <?php echo $r->Status;?></span></div>-->
					<div style="clear:left;"><span class="t24-det-name">Price:</span><span class="t24-det-val t24-mls-CurrentPrice"> <?php echo ' $'.number_format($r->CurrentPrice); ?></span></div>
					<div style="clear:left;"><span class="t24-det-name">Beds:</span> <?php echo $r->BedsTotal.'   Baths: '.$r->BathsFull.' / '.$r->BathsHalf; ?></span></div>
					<!--<div><span class="t24-det-name">Neighbourhood:</span><span class="t24-det-val t24-mls-SubdComplex"> <?php echo $r->SubdComplex;?></span></div>-->
					
					<div style="margin-top:2em;"><h2>Description:</h2></div>
					<div><span class="t24-det-val t24-mls-PublicRemarksConsumerView"><?php echo $r->PublicRemarksConsumerView;?></span></div>
				</div> <?php
			
		}
		
		
		function add_user_children(){
			global $wpdb;
			
			$role_obj = new Aspk_Rets_Roles();
			$users = get_users();
			$parent_user_id = $_POST['hidden_parent_user_id'];
			$uid = $_POST['parent_user_id'];
			if(!$uid){
				$uid = $_POST['hidden_parent_user_id'];
			}
			
			if(isset($_POST['assigned_user_ids'])){
				$children_uid = $_POST['assigned_user_ids'];
				foreach($children_uid as $child_uid){
					$role_obj->add_child($parent_user_id, $child_uid);
				}
			}
			if(isset($_POST['not_assigned_user_ids'])){
				$children_uid = $_POST['not_assigned_user_ids'];
				foreach($children_uid as $child_uid){
					$role_obj->remove_child($parent_user_id, $child_uid);
				}
			}
			
			?>
			<div style="width: 50em;float:left;clear:left;padding:1em;background-color:#FFFFFF;margin-top: 1em;"> 
				<?php if(isset($_POST['save_user'])){ ?>
					<div id="rets_save_message" class="updated" style="float:left;clear:left;">
						Settings have been saved
					</div>
				<?php } ?>
				
				<div style="float:left;clear:left;">
					<h3>Assignment of Users</h3>
				</div>
				<form id="" action="" method="post">
					<input type="hidden" name="hidden_parent_user_id" value="<?php echo $uid; ?>"/>
					<div style="float:left;clear:left;">
						<select style="width:18em;" name="parent_user_id">
							<option value="">--select--</option>
							<?php foreach($users as $user){ 
								$user_role = get_user_meta($user->data->ID, $wpdb->prefix.'capabilities', true);
								$role = array_keys($user_role);
								if($role[0] == 'agent' || $role[0] == 'broker' ||$role[0] == 'staff'){ ?>
									<option value="<?php echo $user->data->ID; ?>"><?php echo $user->data->user_nicename; ?></option>
								<?php } ?>
								
							<?php } ?>
						</select>
					</div>
					<div style="float:left;">
						<input type="submit" value="Go" name="select_parent_user"/>
					</div>
					<?php
					if($_POST['select_parent_user'] || $_POST['save_user']){
						if($_POST['parent_user_id']){
							$uid = $_POST['parent_user_id'];
						}else{
							$uid = $_POST['hidden_parent_user_id'];
						}
						if(!$uid) return;
						$user_data = get_userdata($uid);
						$childrens = $role_obj->get_children($uid);
						?>
						<div style="color:red;float:left;clear:left;" id="change_user">
							<div style="float:left;clear:left;">
								<h4 style="color:#000000;">Changing Assignment of Clients and Prospects for <?php echo $user_data->data->user_nicename; ?>.</h4>
							</div>
							<div style="float:left;clear:left;margin-top:2em;">
								<div style="float:left;"> 
									<select name="not_assigned_user_ids[]" id="left_users" size="10" style="width:18em;" multiple="multiple">
									<?php 
										foreach($users as $user){
											if($childrens){
												if(in_array($user->data->ID,$childrens)){
													continue;
												}
											}
											$user_role = get_user_meta($user->data->ID, $wpdb->prefix.'capabilities', true);
											$role = array_keys($user_role);
											if($role[0] == 'client' || $role[0] == 'prospect'){?>
												<option value="<?php echo $user->data->ID; ?>"><?php echo $user->data->user_nicename; ?></option><?php
											}
										} ?>
									</select>
								</div>
								<div style="margin-left:1em;float:left;margin-top: 7em;"><input style="width: 3em;padding:0.5em;" onClick="move_user_right()" type="button" value=">"/></div>
								<div style="margin-left: 1em;margin-right:1em;float:left;margin-top: 7em;"><input style="width: 3em;padding:0.5em;" onClick="move_user_left()" type="button" value="<"/></div>
								<div style="float:left;"> 
									<select name="assigned_user_ids[]" id="right_users" size="10" style="width:18em;" multiple="multiple">
									<?php if($childrens){
										foreach($childrens as $children_id){
											$user_data = get_userdata($children_id); ?>
											<option value="<?php echo $children_id; ?>"><?php echo $user_data->data->user_nicename; ?></option>
										<?php }
										} ?>
									</select>
								</div>
								<div style="display:none;clear:both;float:left;" id="save_message"> 
									<h4 style="color:red;">Please click on Save to save changes.</h4>
								</div>
								<div style="float:left;clear:left;"> 
									<input type="submit" value="Save" name="save_user"/>
								</div>
							</div>
						</div>
					<?php } ?>
				</form>
			</div>
			<script>
				function move_user_right(){
					 jQuery('#left_users option:selected').each(function() {
						 var opt = jQuery(this).text();
						 var cval = jQuery(this).val();
						 jQuery('#right_users').append('<option   value="'+cval +'" >'+opt+'</option>');
					 });
					 jQuery('#left_users option:selected').remove();
					 jQuery('#right_users option').attr('selected','selected');
					 jQuery('#save_message').show();
				}
				function move_user_left(){
					jQuery('#right_users option:selected').each(function() {
						 var opt = jQuery(this).text();
						 var cval = jQuery(this).val();
						 jQuery('#left_users').append('<option   value="'+cval +'" >'+opt+'</option>');
					});
					 jQuery('#right_users option:selected').remove();
					 jQuery('#left_users option').attr('selected','selected');
					 jQuery('#save_message').show();
				}
				setTimeout(function(){ 
					jQuery('#rets_save_message').hide();
				}, 8000);
			</script>
			<?php
						
		}
		
		function save_search(){
			
			$saved_by = get_current_user_id();
			$name = $_POST['search_name'];
			if(isset($_POST['cbarr'])){
				$uids_array = $_POST['cbarr'];
			}
			$kv = $_SESSION['aspk_search_fields'];
			if(empty($kv)){
				$ret = array('st' => 'er','msg'=>'Error: Cannot Save');
				echo json_encode($ret);
				exit;
			}
			$s = new Aspk_Rets_Search();
			if($_POST['x_search_id']){
				$sid = $_POST['x_search_id'];
				$s->update_search($_POST['x_search_id'], $kv );
				$search_ids[] = $_POST['x_search_id'];
				$saved_time = get_user_meta($saved_by,'__aspk_save_searched_time',true);
				$saved_time[$sid] = date('Y-m-d\TH:i:s', time());
				update_user_meta($saved_by,'__aspk_save_searched_time',$saved_time);
				$ret = array('st' => 'ok','msg'=>'Update Search Successfully','sids'=>$search_ids);
				echo json_encode($ret);
				exit;
			}else{
				//save new search
				$search_ids = array();
				if(empty($uids_array)){
					$uids_array[] = $saved_by;
					$sid = $s->save_search($saved_by, $kv , $name, $saved_by);
					$search_time = get_user_meta($saved_by,'__aspk_save_searched_time',true);
					$search_time[$sid] = date('Y-m-d\TH:i:s', time());
					update_user_meta($saved_by,'__aspk_save_searched_time',$search_time);
					$search_ids[] = $sid;
				}else{
					foreach($uids_array as $selected_uid){
						$sid = $s->save_search($selected_uid, $kv , $name, $saved_by);
						$search_ids[] = $sid;
						$search_time = get_user_meta($selected_uid,'__aspk_save_searched_time',true);
						$search_time[$sid] = date('Y-m-d\TH:i:s', time());
						update_user_meta($selected_uid,'__aspk_save_searched_time',$search_time);
					}
					
				}
				$ret = array('st' => 'ok','msg'=>'Search Saved','sids'=>$search_ids);
				echo json_encode($ret);
				exit;
			}
		}
		
		
		function share_propery_results(){
			$log = new Aspk_Rets_Log();
			$email_obj = new Aspk_Rets_Email(true);
			
			$mls_id = $_POST['mls_id'];
			$recipient_email = $_POST['recipient_email'];
			$recipient_name = $_POST['recipient_name'];
			$user_id = get_current_user_id();
			
			$status = 1;
			$datetime = date('Y-m-d H:i:s');
			$subject = "Shared Property";
			$message = "Dear $recipient_name a property has been shared with you for visit please click on this link  <a href=".$_POST['permalink'].'?mls_id='.$mls_id.">View Shared Property</a>";
			
			if($user_id){
				$user_info = get_userdata($user_id);
				if($user_info->display_name){
					$name = $user_info->display_name;
				}elseif($user_info->user_nicename){
					$name = $user_info->user_nicename;
				}else{
					$name = $user_info->user_login;
				}
				$from = "From: $name <".$user_info->user_email.">\r\n";
			}else{
				$user_id = 0;
				$from = "From:".get_bloginfo('name')."\r\n";
			}
			
			if (!filter_var($recipient_email, FILTER_VALIDATE_EMAIL)) {
			  $ret = array('st' => 'err','msg'=>'Invalid email format');
			  echo json_encode($ret);
			  exit;
			}
			
			if($mls_id && $recipient_email && $recipient_name){
				$count = $log->select_shared_link($mls_id,$recipient_email);
				if($count >0){
					$ret = array('st' => 'err','msg'=>'This Property has already been shared to this email');
					echo json_encode($ret);
				}else{
					$log->insert_share_property_log($user_id,$mls_id,$recipient_name,$recipient_email,$datetime,$status,$subject,$message);
					$email_obj->send_share_property_email($recipient_email,$subject,$message,$from);
					$ret = array('st' => 'ok','msg'=>'Property has been shared successfully');
					echo json_encode($ret);
				}
			}
			exit;
		}
		
		function old_init(){
			
			if(! session_id()){
				session_start();
			}
		}
		
		function remove_favourite(){
			$uid = get_current_user_id();
			if($uid < 1){
				$ret = array('st' => 'err','msg'=>'UnAuthorized User');
				echo json_encode($ret);
				exit;
			}
			$rs1 = new Aspk_Rets_Search();
			$favorite_searches = 	$rs1->get_favorite_by_user($uid);
			$key = array_search($_POST['mls_id'], $favorite_searches);
			unset($favorite_searches[$key]);
			$rs1->after_remove_favourite_save($uid,serialize($favorite_searches));
			$ret = array('st' => 'ok','msg'=>'Search Remove to  Favorite Successfully');
			echo json_encode($ret);
			exit;
		}
		
		function add_favourite(){
			$uid = get_current_user_id();
			if($uid < 1){
				$ret = array('st' => 'err','msg'=>'UnAuthorized User');
				echo json_encode($ret);
				exit;
			}
			$rs1 = new Aspk_Rets_Search();
			$hide_searches = $rs1->get_hidden_by_user($uid);
			$key = array_search($_POST['mls_id'], $hide_searches);
			if($key){
				unset($hide_searches[$key]);
				$rs1->after_favourite_remove_hiden_section($uid,serialize($hide_searches));
			}
			$rs1->save_to_favorite($uid, $_POST['mls_id']);
			$ret = array('st' => 'ok','msg'=>'Search Added to  Favorite Successfully');
			echo json_encode($ret);
			exit;
		}
		
		function hide_property(){
			$uid = get_current_user_id();
			if($uid < 1){
				$ret = array('st' => 'err','msg'=>'UnAuthorized User');
				echo json_encode($ret);
				exit;
			}
			$rs1 = new Aspk_Rets_Search();
			$rs1->hide_property($uid, $_POST['mls_id']);
			$favorite_searches = 	$rs1->get_favorite_by_user($uid);
			if(empty($favorite_searches)) $favorite_searches = array();
			$key = array_search($_POST['mls_id'], $favorite_searches);
			if($key){
				unset($favorite_searches[$key]);
				$rs1->after_remove_favourite_save($uid,serialize($favorite_searches));
			}
			$ret = array('st' => 'ok','msg'=>'Hide Property Successfully');
			echo json_encode($ret);
			exit;
		}
		
		function unhide_property(){
			$uid = get_current_user_id();
			if($uid < 1){
				$ret = array('st' => 'err','msg'=>'UnAuthorized User');
				echo json_encode($ret);
				exit;
			}
			$rs1 = new Aspk_Rets_Search();
			$hide_searches = $rs1->get_hidden_by_user($uid);
			if(empty($hide_searches)) $hide_searches = array();
			$key = array_search($_POST['mls_id'], $hide_searches);
			unset($hide_searches[$key]);
			$rs1->after_favourite_remove_hiden_section($uid,serialize($hide_searches));
			$ret = array('st' => 'ok','msg'=>'Unhide Property Successfully');
			echo json_encode($ret);
			exit;
		}
		
		function init(){

			if (! is_user_logged_in() ) {  
				$url = $_SERVER['SERVER_NAME'];
			
				if (!isset($_COOKIE['aspkflogin'])) {  
					setcookie("aspkflogin", 'yes', 0, "/", $url); 
				}
				
				if(isset( $_POST['aspk_search'])){
					if(isset($_COOKIE['scnt'])){
						$scnt = intval($_COOKIE['scnt']);
					}else{
						$scnt = 0;
					}
					$scnt++;
					
					setcookie("scnt", $scnt, time()+7776000, "/", $url);
				}
			}
			ob_start();
		}
		
		function create_cms_contact($user_id){
			//add integration here
		}
		
		
		
		function wp_footer(){
			
		}
		
		
		function wp_head(){
			?>
				<meta charset="utf-8">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
			<?php
		}
		
		function wp_login($user_login, $user){
			$manage_user = new Aspk_User_Management();
			$user1 = $manage_user->get_user_info($user->data->ID);
			if($user1 == false) $manage_user->isert_wp_user_idx($user->data->ID);
			if (isset($_COOKIE['aspkflogin'])) {
				$url = $_SERVER['SERVER_NAME'];
				ob_end_clean();
				unset($_COOKIE['aspkflogin']);
				unset($_COOKIE['scnt']);
				setcookie('aspkflogin', null, -1, '/', $url);
				setcookie('scnt', null, -1, '/', $url);
				?>
					<script>
						function inIframe () {
							try {
								return window.self !== window.top;
							} catch (e) {
								return true;
							}
						}
						if(inIframe() === true){
							self.parent.location.reload();
						}else{
							window.location.reload();
						}
					</script>
				<?php
				exit();
			}
		}
		
		function enqueue_scripts(){
			
			wp_enqueue_script('jquery');
			wp_enqueue_script('jquery-ui');
			wp_enqueue_script('jquery-ui-dialog');
			wp_enqueue_script( 'jssor', plugins_url( 'js/jssor.js', __FILE__ ),array('jquery'));
			wp_enqueue_script( 'tabsjs', plugins_url( 'js/tabs.js', __FILE__ ),array('jquery'));
			wp_enqueue_script( 'jssor-slider', plugins_url( 'js/jssor.slider.js', __FILE__ ),array('jquery','jssor'));
			wp_enqueue_script( 'jquery-ui-autocomplete', '',array('jquery','jquery-ui-core'));
			wp_enqueue_style( 'autocomplete', plugins_url( 'css/jquery-ui.css', __FILE__ ));
			wp_enqueue_style( 'aspk_tabs', plugins_url( 'css/tabs.css', __FILE__ ));
			wp_enqueue_style( 'fe', plugins_url( 'css/fe.css', __FILE__ ));
			wp_enqueue_script('thickbox', null,  array('jquery'));
			//wp_enqueue_style('thickbox.css', includes_url('/js/thickbox/thickbox.css'), null, '4.2');
			
		}
		
		function login_enqueue_scripts(){
			wp_enqueue_style( 'fe', plugins_url( 'css/fe.css', __FILE__ ));
		}
		
		function admin_enqueue_scripts(){
			wp_enqueue_script( 'jquery');
		}
		
		
		function admin_menu(){
			add_menu_page( 'Assignment of Users', 'Assignment of Users', 'manage_options', 'add_user_children', array(&$this, 'add_user_children'));
			add_options_page( 'RETS Settings', 'RETS Settings', 'manage_options', 'aspk_rets_settings', array(&$this, 'rets_settings'));
		}
		
		function user_children_list($child_uids,$curent_uid){
		?>
			<form method="post" action="">
				<div style="float:left;clear:left">
					<select style="width:20em;" name="view_dashboard_as">
						<?php $curent_data = get_userdata($curent_uid);?>
						<option value="<?php echo $curent_data->ID;?>"><?php echo $curent_data->data->user_nicename; ?></option>
						<?php 
						if($child_uids){
							foreach($child_uids as $child_uid){ 
								$user_data = get_userdata($child_uid); ?>
								<option <?php selected($_POST['view_dashboard_as'], $child_uid ); ?> value="<?php echo $child_uid;?>"><?php echo $user_data->data->user_nicename; ?></option><?php 
							}
						} ?>
					</select>
				</div>
				<div style="margin-left:1em;float:left;">
					<input type="submit" name="" value="submit"/>
				</div>
			</form><?php
		}
		
		function view_dashboard_as_agent($user_id = ''){
			$manage_user = new Aspk_User_Management();
			$uid = get_current_user_id();
			$user_info = $manage_user->get_user_info($uid);
			$log_obj = new Aspk_Rets_Log();
			$user_info = $manage_user->get_user_info($user_id);
			?>
			<div id="tabs" style = "border:none !important;float:left;clear:left;">
			  <ul>
				<li><a style="color:#000000 !important;" href="#tabs-1">Profile</a></li>
				<li><a style="color:#000000 !important;" href="#tabs-2">Saved Properties</a></li>
				<li><a style="color:#000000 !important;" href="#tabs-3">Hidden Properties</a></li>
				<li><a style="color:#000000 !important;" href="#tabs-4">Saved Search List</a></li>
				<li><a style="color:#000000 !important;" href="#tabs-5">Search Notification List</a></li>
				<li><a style="color:#000000 !important;" href="#tabs-6">Email List</a></li>
			  </ul>
			  <div id="tabs-1">
				<?php 
				if($user_info){
					$this->user_update_info($user_info);
				}else{ ?>
					<div style = "float:left:clear:left;color:red;">No User Info</div>
				<?php }				
				?>
			  </div>
			  <div id="tabs-2">
				<?php
				$result = $this->saved_searches($user_id); 
				if($result){
					$this->show_saved_searches($result,'favourite'); 
				}else{ ?>
					<div style = "float:left:clear:left;color:red;">No  Favorite Properties</div>
				<?php }				
				?>
			  </div>
			  <div id="tabs-3">
				<?php
				$hide_result = $this->hide_searches_arr($user_id);
				if($hide_result){				
					$this->show_saved_searches($hide_result,'hide');
				}else{ ?>
					<div style = "float:left:clear:left;color:red;">No Hidden Properties</div>
				<?php }				
				?>
			  </div>
			  <div id="tabs-4">
				<?php $this->saved_search_list($user_id); ?>
			  </div>
			  <div id="tabs-5">
				<?php $this->search_notification_list($user_id); ?>
			  </div>
			  <div id="tabs-6" style="float:left;clear:left;background:#CCCCCC;"><?php 
				$emails = $log_obj->select_share_search_results_log($user_id);
					if($emails){
						foreach($emails as $email){ 
							$user = get_user_by( 'id', $email->user_id);
							if($user->data->display_name){
								$name = $user->data->display_name;
							}elseif($user->user_nicename){
								$name = $user->user_nicename;
							}elseif($user->user_login){
								$name = $user->user_login;
							}else{
								$name = "Unknown";
							} ?>
							<div style="width: 32em;float:left;clear:left;background:#FFFFFF;padding:1em;">
								<div style="float:left;clear:left;margin-bottom:2em;">
									<div style="float:left;clear:left;">
										<div style="float:left;"><?php 
											if($email->status == 1){
												echo "<h2>Shared Property</h2>";
											}else{
												echo "<h2>Shared Search</h2>";
											}
										?></div>
									</div>
									<div style="float:left;clear:left;">
										<div style="float:left;">Date Sent:</div>
										<div style="float:left;margin-left:1em;"><?php echo $email->datetime; ?></div>
									</div>
									<div style="float:left;clear:left;">
										<div style="float:left;">Sender Name:</div>
										<div style="float:left;margin-left:1em;"><?php echo $name; ?></div>
									</div>
								</div>
								<div style="float:left;clear:left;">
									<input id="view_mail_content_<?php echo $email->id; ?>" value="View" type="button" onClick="view_mail_content_<?php echo $email->id; ?>(this)"/>
									<input style="display:none;" id="close_mail_content_<?php echo $email->id; ?>" value="Close" type="button" onClick="close_mail_content_<?php echo $email->id; ?>(this)"/>
								</div>
								<div style="margin-top: 1em;overflow:scroll;height:14em;width:30em;background:#FFFFFF;display:none;float:left;clear:left;" id="email_content_<?php echo $email->id; ?>">
									<div style="float:left;clear:left;">
										<div style="float:left;">Subject:</div>
										<div style="float:left;margin-left:1em;"><?php echo $email->subject; ?></div>
									</div>
									<div style="float:left;clear:left;">
										<div style="float:left;">Body:</div>
										<div style="float:left;margin-left:1em;"><?php echo $email->body; ?></div>
									</div>
									<div style="float:left;clear:left;">
										<div style="float:left;">Recipient Name:</div>
										<div style="float:left;margin-left:1em;"><?php echo $email->recipient_name; ?></div>
									</div>
									<div style="float:left;clear:left;">
										<div style="float:left;">From:</div>
										<div style="float:left;margin-left:1em;"><?php echo $user->user_email; ?></div>
									</div>
									<div style="float:left;clear:left;">
										<div style="float:left;">To</div>
										<div style="float:left;margin-left:1em;"><?php echo $email->recipient_email; ?></div>
									</div>
								</div>
							</div>
							<script>
								function view_mail_content_<?php echo $email->id; ?>(x){
									jQuery('#email_content_<?php echo $email->id; ?>').show();
									jQuery(x).hide();
									jQuery('#close_mail_content_<?php echo $email->id; ?>').show();
									
								}
								function close_mail_content_<?php echo $email->id; ?>(x){
									jQuery('#email_content_<?php echo $email->id; ?>').hide();
									jQuery(x).hide();
									jQuery('#view_mail_content_<?php echo $email->id; ?>').show();
								}
							</script>
						<?php }
					}else{ ?>
					<div style = "float:left:clear:left;color:red;">No Email List</div><?php 
					}
				?>
			  </div>
			</div>
				<script>
						jQuery(function() {
							jQuery("#tabs").tabs();
						});
				</script>
			<?php
		}
		
		function search_notification_list($wp_uid){
			$rs1 = new Aspk_Rets_Search();
			$result = $rs1->get_schedule_search_result($wp_uid);
			$page = get_page_by_title('Rets Search'); // change with the respect of live
			$url = $this->get_rets_search_page_link();
			if(!empty($result)){
				?>	
					<div style = "display:none;" id = "save_msg"></div>
					<div style = "float:left;clear:left;color:green;"><h1>Schedule Searches</h1></div>
				<?php
				foreach($result as $rs){
					?>
					<div style = "float:left;clear:left;" id = "srch_<?php echo $rs->id; ?>">
						<div style = "float:left;width:8em;"><?php echo $rs->name; ?></div>
						<div style = "float:left;width:9em;"><?php echo $rs->additional_emails; ?></div>
						<div style = "float:left;width:6em;"><a href = "<?php echo $url.'?srch_id='.$rs->search_id; ?>" >View</a>/<a href = "<?php echo $url.'?edit_id='.$rs->search_id.'&ms'; ?>" >Edit</a></div>
						<div style = "float:left;width:5em;padding: 5px;" ><input onClick = "stop_schedule_search(<?php echo $rs->id; ?>)"type = "button" value = "Stop" /></div>
						<div style = "float:left;width:5em;padding: 5px;" ><input onClick = "delete_schedule_search(<?php echo $rs->search_id; ?>,<?php echo $rs->id; ?>)"type = "button" value = "Delete" /></div>
					</div>
					<?php 
				}
				?>
					<script>
						var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' );?>';
						function stop_schedule_search(schedule_id){
							var data = {
								'action'   : 'aspk_schedule_search',
								'schedule_id' : schedule_id,
							};
								
							jQuery.post(ajaxurl, data, function(response) {
								obj = JSON.parse(response);
								if(obj.st == 'ok'){
									jQuery('#save_msg').html(obj.msg);
									jQuery('#save_msg').dialog();
									jQuery('#srch_'+schedule_id).html('');
								}
							});
						}
						function delete_schedule_search(search_id , schedule_id){
							var data = {
								'action'   : 'aspk_schedule_search_del',
								'schedule_id' : schedule_id,
								'search_id' : schedule_id,
							};
								
							jQuery.post(ajaxurl, data, function(response) {
								obj = JSON.parse(response);
								if(obj.st == 'ok'){
									jQuery('#save_msg').html(obj.msg);
									jQuery('#save_msg').dialog();
									jQuery('#srch_'+schedule_id).html('');
								}
							});
						}
					</script>
				<?php
			}else{
				?><div style = "float:left:clear:left;color:red;">No Schedule Search List Found</div><?php
			}
		}
		
		function user_dashboard(){
			global $wpdb;
			
			$roles_obj = new Aspk_Rets_Roles();
			
			if (! is_user_logged_in() ) {
				return "Please login <a href='".home_url()."/wp-login.php'>here</a>";
			}
						
			$manage_user = new Aspk_User_Management();
			if(isset($_POST['update_profile'])){
				$manage_user->update_info($_POST['wp_user_id'],$_POST['user_name'],$_POST['email'],$_POST['phone'],$_POST['h_st'],$_POST['h_cty'],$_POST['h_stat'],$_POST['h_zip'],$_POST['w_st'],$_POST['w_cty'],$_POST['w_stat'],$_POST['w_zip']);
				$manage_user->wp_email_update($_POST['wp_user_id'],$_POST['email']);
			}
			$user_info = $manage_user->get_user_info(get_current_user_id());
			$wp_info = get_user_by('id',$user_info->wp_user_id);
			if($user_info == false) return "No user info found";
			
			$user_id = get_current_user_id();
			$user_role = get_user_meta($user_id, $wpdb->prefix.'capabilities', true);
			$role = array_keys($user_role);
			$childerns = $roles_obj->get_children($user_id);
			if($role[0] == 'agent' || $role[0] == 'broker' || $role[0] == 'staff'){
				$this->user_children_list($childerns,$user_id);
				if(isset($_POST['view_dashboard_as'])){
					$user_id = $_POST['view_dashboard_as'];
					$this->view_dashboard_as_agent($user_id);
				}else{
					$this->view_dashboard_as_agent($user_id);
				}
			}else{
				?>
				<div id="tabs" style = "border:none !important;">
				  <ul>
					<li><a style="color:#000000 !important;" href="#tabs-1">Profile</a></li>
					<li><a style="color:#000000 !important;" href="#tabs-2">Saved Properties</a></li>
					<li><a style="color:#000000 !important;" href="#tabs-3">Hidden Properties</a></li>
					<li><a style="color:#000000 !important;" href="#tabs-4">Saved Search List</a></li>
					<li><a style="color:#000000 !important;" href="#tabs-5">Search Notification List</a></li>
				  </ul>
				  <div id="tabs-1">
					<?php $this->user_update_info($user_info); ?>
				  </div>
				  <div id="tabs-2">
					<?php $result = $this->saved_searches(get_current_user_id()); ?>
					<?php $this->show_saved_searches($result,'favourite'); ?>
				  </div>
				  <div id="tabs-3">
					<?php $hide_result = $this->hide_searches_arr(get_current_user_id()); ?>
					<?php $this->show_saved_searches($hide_result,'hide'); ?>
				  </div>
				  <div id="tabs-4">
					<?php $this->saved_search_list(get_current_user_id()); ?>
				  </div>
				  <div id="tabs-5">
					<?php $this->search_notification_list($user_id); ?>
				  </div>
				</div>
				<script>
					//jQuery( document ).ready(function() {
						jQuery(function() {
							jQuery("#tabs").tabs();
						});
					//});
				</script>
				<?php
			}
		}
		
		function get_rets_search_page_link(){
			global $wpdb;
			
			$query = "SELECT ID FROM {$wpdb->prefix}posts where post_content LIKE '%[aspk_rets_search]%' AND post_type = 'page' AND post_status='publish'"; 
			$results = $wpdb->get_col($query);
			if($results){
				foreach($results as $post_id){
					$guid = get_permalink($post_id);
					return $guid;
				}
			}
			
		}
		
		function saved_search_list($uid){
			global $wpdb;
			
			$rs1 = new Aspk_Rets_Search($uid);
			$current_user_searches = $rs1->get_my_save_search($uid);
			$user_role = get_user_meta($uid, $wpdb->prefix.'capabilities', true);
			$url = $this->get_rets_search_page_link();
			if($user_role){
				$role = array_keys($user_role);
				$aspk_role = $role[0];
			} ?>
			<div style = "float:left;clear:left;color:green;"><h1>Saved Searches</h1></div>
				<div style = "float:left;clear:left;color:green;"><h2>My Search</h2></div>
			<?php if($current_user_searches){ ?>
				<?php
				foreach($current_user_searches as $current_user_search){ 
				?>
					<div style = "float:left;clear:left;" id = "srch_<?php echo $current_user_search->id; ?>">
						<div style = "float:left;width:19em;"><?php echo $current_user_search->name; ?></div>
						<div style = "float:left;width:6em;"><a href = "<?php echo $url.'?srch_id='.$current_user_search->id; ?>" >View</a>/<a href = "<?php echo $url.'?edit_id='.$current_user_search->id.'&ms'; ?>" >Edit</a></div>
						<div style = "float:left;width:5em;padding: 5px;" ><input onClick = "delete_save_search(<?php echo $current_user_search->id; ?>)"type = "button" name = "del" value = "Delete" /></div>
					</div>
					<?php
					$this->delete_save_search_script($current_user_search->id);
				}	
			}else{ ?>
				<div style = "float:left;clear:left;color:red;"><small>MY Search Not Found</small></div><?php
			}  ?>
			
			<div style = "float:left;clear:left;color:green;"><h2>Searches Shared With Me</h2></div><?php
			$shared_searches = $rs1->get_shared_searches($uid);
			if($shared_searches){
				foreach($shared_searches as $shared_search){ ?>
					<div style = "float:left;clear:left;" id = "srch_<?php echo $shared_search->id; ?>">
						<div style = "float:left;width:19em;"><?php echo $shared_search->name; ?></div>
						<div style = "float:left;width:6em;"><a href = "<?php echo $url.'?srch_id='.$shared_search->id; ?>" >View</a>/<a href = "<?php echo $url.'?edit_id='.$shared_search->id.'&ms'; ?>" >Edit</a></div>
						<div style = "float:left;width:5em;padding: 5px;" ><input onClick = "delete_save_search(<?php echo $shared_search->id; ?>)"type = "button" name = "del" value = "Delete" /></div>
					</div> <?php
					$this->delete_save_search_script($shared_search->id);
				}	
			
			}else{
				?><div style = "float:left;clear:left;color:red;"><small>Shared Search Not Found</small></div><?php
			}
			
			
			if($aspk_role == 'agent' || $aspk_role == 'staff' || $aspk_role == 'broker'){ ?>
				<div style = "float:left;clear:left;color:green;"><h2>Terrace 24 Search</h2></div><?php
				$terrace_user_id = $rs1->get_user_id_by_email('terrace@gmail.com');
				$current_user_searches = $rs1->get_saved_search_of_user($terrace_user_id);
				if($current_user_searches){
					foreach($current_user_searches as $current_user_search){ ?>
						<div style = "float:left;clear:left;" id = "srch_<?php echo $current_user_search->id; ?>">
							<div style = "float:left;width:19em;"><?php echo $current_user_search->name; ?></div>
							<div style = "float:left;width:6em;"><a href = "<?php echo $url.'?srch_id='.$current_user_search->id; ?>" >View</a>/<a href = "<?php echo $url.'?edit_id='.$current_user_search->id.'&ms'; ?>" >Edit</a></div>
							<div style = "float:left;width:5em;padding: 5px;" ><input onClick = "delete_save_search(<?php echo $current_user_search->id; ?>)"type = "button" name = "del" value = "Delete" /></div>
						</div> <?php
						$this->delete_save_search_script($current_user_search->id);
					}	
				}else{
					?><div style = "float:left;clear:left;color:red;"><small>Terrace Search Not Found</small></div><?php
				}	
			} ?>
			
			<div style = "float:left;clear:left;color:green;"><h2>Public Search</h2></div><?php
			$terrace_user_id = $rs1->get_user_id_by_email('public@gmail.com');
			$current_user_searches = $rs1->get_saved_search_of_user($terrace_user_id);
			if($current_user_searches){
				foreach($current_user_searches as $current_user_search){ ?>
					<div style = "float:left;clear:left;" id = "srch_<?php echo $current_user_search->id; ?>">
						<div style = "float:left;width:19em;"><?php echo $current_user_search->name; ?></div>
						<div style = "float:left;width:6em;"><a href = "<?php echo $url.'?srch_id='.$current_user_search->id; ?>" >View</a>/<a href = "<?php echo $url.'?edit_id='.$current_user_search->id.'&ms'; ?>" >Edit</a></div>
						<div style = "float:left;width:5em;padding: 5px;" ><input onClick = "delete_save_search(<?php echo $current_user_search->id; ?>)"type = "button" name = "del" value = "Delete" /></div>
					</div> <?php
					$this->delete_save_search_script($current_user_search->id);
				}	
			}else{
				?><div style = "float:left;clear:left;color:red;"><small>Public Search Not Found</small></div><?php
			}
		}
		
		function delete_save_search_script($sid){
			?>
			<script>
				var sid = <?php echo $sid; ?>;
				var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' );?>';
				function delete_save_search(sid){
					var txt;
					var r = confirm("Do you want to delete search!");
					if (r == true) {
						var data = {
							'action'   : 'aspk_search_del',
							'sid' : sid,
						};
						jQuery.post(ajaxurl, data, function(response) {
							obj = JSON.parse(response);
							if(obj.st == 'ok'){
								jQuery('#srch_'+sid).html(obj.msg);
							}
						});
					}else {
						return false;
					}
					
				}
			</script>
			<?php
		}
		
		function hide_searches_arr($uid){
			$result = array();
			$ctl = new Aspk_Rets_Wp_Controller();
			$rs1 = new Aspk_Rets_Search();
			//$uid = get_current_user_id();
			$hide_searches = $rs1->get_hidden_by_user($uid);
			if(!empty($hide_searches)){
				foreach($hide_searches as $hs){
					$matrix = $ctl->mls_to_matrix(intval($hs));
					$raw = $ctl->get_property($matrix);
					if($ctl->records_found() > 0){
						$r = json_decode($raw);	
						$result[$matrix] = $r;
					}
				}
				return $result;
			}
		}
		
		function show_saved_searches($results , $action){
			$uid = get_current_user_id();
			$ctl = new Aspk_Rets_Wp_Controller();
			$sc = new Aspk_Rets_Search();
			$fav_properties = $sc->get_favorite_by_user($uid);
			//return;
			if(!empty($results)){
				foreach($results as $r){
					
					$img = $ctl->get_images($r->Matrix_Unique_ID);
						
					?>
			
					<div style="clear:left;float:left;margin-bottom:1em;" id="prop_<?php echo $r->MLSNumber; ?>">
						<div style="float:left;"><div style="width:15em;">
						<?php if(isset($img[0]['Data'])){?>
							<a href="<?php echo $this->get_rets_search_page_link().'?propid='.$r->Matrix_Unique_ID.'&mls_id='.$r->MLSNumber;?>"><img src="data:image/jpeg;base64,<?php echo base64_encode($img[0]['Data']);?>" width="400"/></a>
						<?php }else{ ?>
							<img src="<?php echo plugins_url( 'img/no_image.png', __FILE__ );?>" alt="No Image Available" width="400">
						<?php } ?>
						</div></div>
						<div style="clear:left;float:left;"><span class="t24-det-val t24-mls-StreetNumber"><?php echo $r->StreetNumber.'</span><span class="t24-det-val t24-mls-StreetDirPrefix"> '.$r->StreetDirPrefix.'</span><span class="t24-det-val t24-mls-StreetName"> '.$r->StreetName.'</span><span class="t24-det-val t24-mls-StreetSuffix"> '.$r->StreetSuffix.'</span><span class="t24-det-val t24-mls-StreetDirSuffix"> '.$r->StreetDirSuffix;?></span></div>
						<div style="clear:left;<?php if(empty($r->UnitNumber)) echo 'display:none;';?>"><span class="t24-det-name">Unit #</span><span class="t24-det-val t24-mls-UnitNumber"> <?php echo $r->UnitNumber;?></span></div>
						<div style="clear:left;margin-top:1em;"><span class="t24-det-val t24-mls-City"><?php echo $r->City.'</span><span class="t24-det-val t24-mls-PostalCode"> '.$r->PostalCode.'</span><span class="t24-det-val t24-mls-CountyOrParish"> '.$r->CountyOrParish.'</span> County';?></div>
						<!--<div><span class="t24-det-name">Status:</span><span class="t24-det-val t24-mls-Status"> <?php echo $r->Status;?></span></div>-->
						<div style="clear:left;"><span class="t24-det-name">Price:</span><span class="t24-det-val t24-mls-CurrentPrice"> <?php echo ' $'.number_format($r->CurrentPrice); ?></span></div>
						<div style="clear:left;"><span class="t24-det-name">Beds:</span> <?php echo $r->BedsTotal.'   Baths: '.$r->BathsFull.' / '.$r->BathsHalf; ?></span></div>
						<!--<div><span class="t24-det-name">Neighbourhood:</span><span class="t24-det-val t24-mls-SubdComplex"> <?php echo $r->SubdComplex;?></span></div>-->
						
						<div style="margin-top:2em;"><h2>Description:</h2></div>
						<div><span class="t24-det-val t24-mls-PublicRemarksConsumerView"><?php echo $r->PublicRemarksConsumerView;?></span></div>
						
						<?php if($action == 'hide'){ ?>
						<?php /* if(! in_array($r->MLSNumber,$fav_properties)){ ?>
									<div style = "float:left;padding: 1em;" id="mls_<?php echo $r->MLSNumber; ?>"><input onClick = "save_to_favourite(<?php echo $r->MLSNumber; ?>)" type = "button" value = "Save to Favourites"/></div>
								<?php }else{ ?>
									<div style = "float:left;padding: 1em;" >Favourite</div>
								<?php } */ ?>
								<div style = "float:left;padding: 1em;" ><input onClick = "unhide_property(<?php echo $r->MLSNumber; ?>)" type = "button" value = "Unhide"/></div>
						<?php }else{ ?>
						<div style = "float:left;padding: 1em;" id="mls_<?php echo $r->MLSNumber; ?>"><input onClick = "remove_to_favourite(<?php echo $r->MLSNumber; ?>)" type = "button" value = "Unsave"/></div>
						<div style="clear:both"><hr/></div>
						<?php /*<div style = "float:left;padding: 1em;" ><input onClick = "hide_property(<?php echo $r->MLSNumber; ?>)" type = "button" value = "Hide"/></div>  */
							}?>
					</div>
					
				<?php
				}//foreach ends	
			}else{
				?><div style = "float:left; clear:left;color:red;">No Record Found</div><?php
			}
			?>
			<script>
				var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' );?>';
				function save_to_favourite(mls_id){
					var data = {
						'action'   : 'aspk_add_favourite',
						'mls_id' : mls_id,
				
					};
						
						jQuery.post(ajaxurl, data, function(response) {
						obj = JSON.parse(response);
						if(obj.st == 'ok'){
							jQuery('#prop_'+mls_id).hide();
						}
					});
				}
				function remove_to_favourite(mls_id){
					var data = {
						'action'   : 'aspk_remove_favourite',
						'mls_id' : mls_id,
				
					};
						
						jQuery.post(ajaxurl, data, function(response) {
						obj = JSON.parse(response);
						if(obj.st == 'ok'){
							jQuery('#prop_'+mls_id).hide();
						}
					});
				}
				function unhide_property(mls_id){
					var data = {
						'action'   : 'aspk_unhide_property',
						'mls_id' : mls_id,
				
					};
						
						jQuery.post(ajaxurl, data, function(response) {
						obj = JSON.parse(response);
						if(obj.st == 'ok'){
							jQuery('#prop_'+mls_id).hide();
						}
					});
				}
				function hide_property(mls_id){
					var data = {
						'action'   : 'aspk_hide_property',
						'mls_id' : mls_id,
				
					};
						
						jQuery.post(ajaxurl, data, function(response) {
						obj = JSON.parse(response);
						if(obj.st == 'ok'){
							jQuery('#prop_'+mls_id).hide();
							window.location.href = '<?php echo get_permalink().'?ms=yes'; ?>';
						}
					});
				}
				
			</script>
			<?php
		}
		
		function user_update_info($user_info){
			
			$cur_user_data = get_user_by( 'id', get_current_user_id());
			$cur_user_data->data->user_email;
			if(!empty($user_info->email)){
				$email = $user_info->email;
			}else{
				$email = $cur_user_data->data->user_email;
			}
			?>
			<h2>Profile</h2>
				<div style = "float:left; clear:left;">
					<form action="" method="post">
						<div style = "float:left; clear:left; margin:1em;">
							<div style = "float:left; width:10em;">Name</div>
							<input id = "aspk_name" type = "hidden" name = "wp_user_id" value = "<?php echo $user_info->wp_user_id; ?>" />
							<div style = "float:left;"><input type = "text" name = "user_name" value = "<?php echo $user_info->name; ?>" /></div>
						</div>
						<div style = "float:left; clear:left;margin:1em;">
							<div style = "float:left; width:10em;">Email</div>
							<div style = "float:left; "><input type = "text" name = "email" value = "<?php echo $email; ?>" /></div>
						</div>
						<div style = "float:left; clear:left;margin:1em;">
							<div style = "float:left; width:10em;">Phone Number</div>
							<div style = "float:left; "><input id = "aspk_phone" type = "text" name = "phone" value = "<?php echo $user_info->phone; ?>" /></div>
						</div>
						<div style = "float:left; clear:left;margin:1em;">
							<div style = "float:left; width:10em;">Home Street</div>
							<div style = "float:left; "><input type = "text" name = "h_st" value = "<?php echo $user_info->home_street; ?>" /></div>
						</div>
						<div style = "float:left; clear:left;margin:1em;">
							<div style = "float:left; width:10em;">Home City</div>
							<div style = "float:left; "><input type = "text" name = "h_cty" value = "<?php echo $user_info->home_city; ?>" /></div>
						</div>
						<div style = "float:left; clear:left;margin:1em;">
							<div style = "float:left; width:10em;">Home State</div>
							<div style = "float:left; "><input type = "text" name = "h_stat" value = "<?php echo $user_info->home_state; ?>" /></div>
						</div>
						<div style = "float:left; clear:left;margin:1em;">
							<div style = "float:left; width:10em;">Home Zip</div>
							<div style = "float:left; "><input type = "text" name = "h_zip" value = "<?php echo $user_info->home_zip; ?>" /></div>
						</div>
						<div style = "float:left; clear:left;margin:1em;">
							<div style = "float:left; width:10em;">Work Street</div>
							<div style = "float:left; "><input type = "text" name = "w_st" value = "<?php echo $user_info->work_street; ?>" /></div>
						</div>
						<div style = "float:left; clear:left;margin:1em;">
							<div style = "float:left; width:10em;">Work City</div>
							<div style = "float:left; "><input type = "text" name = "w_cty" value = "<?php echo $user_info->work_city; ?>" /></div>
						</div>
						<div style = "float:left; clear:left;margin:1em;">
							<div style = "float:left; width:10em;">Work State</div>
							<div style = "float:left; "><input type = "text" name = "w_stat" value = "<?php echo $user_info->work_state; ?>" /></div>
						</div>
						<div style = "float:left; clear:left;margin:1em;">
							<div style = "float:left; width:10em;">Work Zip</div>
							<div style = "float:left; "><input type = "text" name = "w_zip" value = "<?php echo $user_info->work_zip; ?>" /></div>
						</div>
						<div style = "float:left; clear:left;"><input type = "submit" name = "update_profile" value = "Update" /></div>
					</form>
				</div>
			<?php
		}
			
		function saved_searches($uid){
			$result = array();
			$ctl = new Aspk_Rets_Wp_Controller();
			$rs1 = new Aspk_Rets_Search();
			//$uid = get_current_user_id();
			$favorite_searches = 	$rs1->get_favorite_by_user($uid);
			if(!empty($favorite_searches)){
				foreach($favorite_searches as $fs){
					$matrix = $ctl->mls_to_matrix(intval($fs));
					$raw = $ctl->get_property($matrix);
					if($ctl->records_found() > 0){
						$r = json_decode($raw);	
						$result[$matrix] = $r;
					}
				}
				return $result;
			}	
		}
		
		function rets_settings_default(){
			$defatuls = array();
			$defatuls['rets_end_point'] = '';
			$defatuls['rets_user_id'] = '';
			$defatuls['rets_user_password'] = '';
			return $defatuls;
		}
		
		function rets_settings(){
			$rets_settings = array();
			if(isset($_POST['save_rets_setting'])){
				$rets_end_point = $_POST['rets_end_point'];
				$rets_user_id = $_POST['rets_user_id'];
				$rets_user_password = trim($_POST['rets_user_password']);
				$rets_settings['rets_end_point'] = $rets_end_point;
				$rets_settings['rets_user_id'] = $rets_user_id;
				$rets_settings['rets_user_password'] = $rets_user_password;
				update_option('_aspk_rets_settings',$rets_settings);
			}
			$defatuls = $this->rets_settings_default();
			$saved_data = get_option('_aspk_rets_settings',$defatuls);
			
			?>
				<div style="float:left;clear:left;padding:1em;background-color:#FFFFFF;margin-top: 1em;"> 
					<?php
					if(isset($_POST['save_rets_setting'])){
						?>
						<div id="rets_save_message" class="updated" style="float:left;clear:left;">
							Settings have been saved
						</div>
						<?php
					}
					?>
					<div style="float:left;clear:left;">
						<h3>RETS Settings</h3>
					</div>
					<div style="float:left;clear:left;">
						<form action="" method="post">
							<div style="float:left;clear:left;margin-top:1em;">
								<div style="float:left;width: 5em;">End Point</div>
								<div style="margin-left:1em;float:left;"><input value="<?php if(isset($saved_data['rets_end_point'])){echo $saved_data['rets_end_point'];}?>" required style="width: 20em;" type="url" name="rets_end_point"/> </div>
							</div>
							<div style="float:left;clear:left; margin-top:1em;">
								<div style="float:left;width: 5em;">User id</div>
								<div style="margin-left:1em;float:left;"><input value="<?php if(isset($saved_data['rets_user_id'])){echo $saved_data['rets_user_id'];}?>" required style="width: 20em;"  type="text" name="rets_user_id"/> </div>
							</div>
							<div style="float:left;clear:left;margin-top:1em;">
								<div style="float:left;width: 5em;">Password</div>
								<div style="margin-left:1em;float:left;"><input value="<?php if(isset($saved_data['rets_user_password'])){echo $saved_data['rets_user_password'];}?>" required style="width: 20em;"  type="password" name="rets_user_password"/> </div>
							</div>
							<div style="float:left;clear:left;margin-top:1em;">
								<div style="float:left;">
									<input class="button button-primary" type="submit" name="save_rets_setting" value="Save"/>
								</div>
							</div>
						</form>
					</div>
				</div>
				<script>
					setTimeout(function(){ 
						jQuery('#rets_save_message').hide();
					}, 8000);
				</script>
			<?php
		}
		
		function install(){
			$name = "Terrace 24";
			$email = "terrace@gmail.com";
			$password = "Te*(73)jdyE";
			$this->create_wp_users($name,$email,$password);
			$name = "Public";
			$email = "public@gmail.com";
			$password = "Pu%*RY8Q)";
			$this->create_wp_users($name,$email,$password);
			$name = "annoymous";
			$email = "annoymous@gmail.com";
			$password = "98%*RY}Q)";
			$this->create_wp_users($name,$email,$password);
		}
		
		function create_wp_users($name,$email,$password){
			$user_id = username_exists($name);
			if (!$user_id and email_exists($email) == false ) {
				$user_id = wp_create_user( $name, $password, $email );
			}else{
				
			}
		}
		
		function basic_rets(){
			if(isset( $_POST['aspk_search']) || (isset($_GET['mid']) && isset($_GET['pg'])) || isset($_GET['srch_id'])){
				$this->search_result_page();
				return;
			}elseif((isset($_GET['propid']))){
				$this->detail_page(intval($_GET['propid']));
				return;
			}elseif((isset($_GET['mls_id']))){
				$ctl = new Aspk_Rets_Wp_Controller();
				if(!$ctl->we_have_settings()){
					echo "Please save settings before proceeding";
					return;
				}
				$matrix = $ctl->mls_to_matrix(intval($_GET['mls_id']));
				$this->detail_page($matrix);
				return;
			}/* elseif(isset($_GET['edit_id'])){
				$sc = new Aspk_Rets_Search();
				$rs = $sc->get_search_by_id( $_GET['edit_id']);
				$_SESSION['aspk_search_fields'] = $rs;
			} */
			$pv = false;
			
			if(session_id()){
				if(! isset($_GET['ms'])){
					$_SESSION['aspk_dmql'] = null;
					$_SESSION['aspk_search_fields'] = null;
				}else{
					$pv = $_SESSION['aspk_search_fields'];
				}
			}
			if(isset($_GET['edit_id'])){
				$sc = new Aspk_Rets_Search();
				$sid = $_GET['edit_id'];
				$results = $sc->get_search_by_id($sid);
				$pv = $results;
			}

			if(isset($_COOKIE['scnt'])){
				$scnt = intval($_COOKIE['scnt']);
			}else{
				$scnt = 0;
			}
			
			require_once(__DIR__ ."/schools.php");
	
			?>
			<?php add_thickbox(); ?>
			<script>
				var url = "<?php echo site_url();?>/wp-login.php?action=register&TB_iframe=true&width=400&height=500";
			</script>
			<?php
				if(! isset($pv['PropertyType-Any'])) $pv['PropertyType-Any'] = "ATT,DET";
				if(! isset($pv['advanced_open'])) $pv['advanced_open'] = "0";
				
			?>
			<form id ="basic_form"  method="post" action="">
				<div style="clear:left;"><span>Property</span>
					<span>
						<select name="PropertyType-Any" >
							<option value="ATT,DET" <?php selected( $pv['PropertyType-Any'], "ATT,DET" ); ?>>House & Condo/Townhouse</option>
							<option value="DET" <?php selected( $pv['PropertyType-Any'], "DET"); ?>>House Only</option>
							<option value="ATT" <?php selected( $pv['PropertyType-Any'], "ATT" ); ?>>Condo/Townhouse Only</option>
						</select>
					</span>
				</div>
				<div style="clear:left;"><span>City</span>
					<span><input  type="text" name="City" value="<?php if(isset($pv['City'])) echo $pv['City'];?>" ></span>
				</div>
				<div style="clear:left;"><span>Zip Code</span>
					<span><input  type="text" name="PostalCode" value="<?php if(isset($pv['PostalCode'])) echo $pv['PostalCode'];?>"></span>
				</div>
				<div style="clear:left;"><span>Min Price</span>
					<span><input  type="text" name="CurrentPrice-From" value="<?php if(isset($pv['CurrentPrice-From'])) echo $pv['CurrentPrice-From'];?>"></span>
				</div>
				<div style="clear:left;"><span>Max Price</span>
					<span><input type="text" name="CurrentPrice-UpTo" value="<?php if(isset($pv['CurrentPrice-UpTo'])) echo $pv['CurrentPrice-UpTo'];?>"></span>
				</div>
				<div style="clear:left;"><span>Beds</span>
					<span>
						<select name="BedsTotal" >
							<option value="" <?php if(! isset($pv['BedsTotal'])){echo 'selected';}else{selected( $pv['BedsTotal'], "" );}?>>Any</option>
							<option value="1+" <?php if( isset($pv['BathsFull'])){ selected( $pv['BedsTotal'], "1+" );} ?>>1+</option>
							<option value="2+" <?php if( isset($pv['BathsFull'])){ selected( $pv['BedsTotal'], "2+" );}?>>2+</option>
							<option value="3+" <?php if( isset($pv['BathsFull'])){ selected( $pv['BedsTotal'], "3+" );} ?>>3+</option>
							<option value="4+" <?php if( isset($pv['BathsFull'])){ selected( $pv['BedsTotal'], "4+" );}?>>4+</option>
							<option value="5+" <?php if( isset($pv['BathsFull'])){ selected( $pv['BedsTotal'], "5+" );} ?>>5+</option>
						</select>
					</span>
				</div>
				<div style="clear:left;"><span>Baths</span>
					<span>
						<select name="BathsFull" >
							<option value="" <?php if(! isset($pv['BathsFull'])){echo 'selected';}else{selected( $pv['BathsFull'], "" );}?>>Any</option>
							<option value="1+" <?php if( isset($pv['BathsFull'])){ selected( $pv['BathsFull'], "1+" );} ?>>1+</option>
							<option value="2+" <?php if( isset($pv['BathsFull'])){ selected( $pv['BathsFull'], "2+" );} ?>>2+</option>
							<option value="3+" <?php if( isset($pv['BathsFull'])){ selected( $pv['BathsFull'], "3+" );} ?>>3+</option>
							<option value="4+" <?php if( isset($pv['BathsFull'])){ selected( $pv['BathsFull'], "4+" );} ?>>4+</option>
							<option value="5+" <?php if( isset($pv['BathsFull'])){ selected( $pv['BathsFull'], "5+" );} ?>>5+</option>
						</select>
					</span>
				</div>
				<!--adv form-->
				<?php
					$stl = 'style="display:none;"';
					if($pv['advanced_open'] == '1'){
						$stl = '';
					} 
				?>
							<div id="aspk_advance_search" <?php echo $stl;?>>
								<div style="clear:left;"><span>Neighborhood/Complex:</span>
									<span><input  type="text" name="SubdComplex-Like" value="<?php if(isset($pv['SubdComplex-Like'])) echo $pv['SubdComplex-Like'];?>"></span>
								</div>
								<div style="clear:left;"><div>Required Features:</div>
									<span><input  id="shortsale" type="checkbox" name="SpecialCircumstances-Any" value="POTSS,SSPAP" <?php if(isset($pv['SpecialCircumstances-Any'])) checked( $pv['SpecialCircumstances-Any'], "POTSS,SSPAP" ); ?> ></span><span>Short Sale</span>
								</div>
								<?php if(isset($_GET['edit_id'])){
									?><input type = "hidden" name = "edit_search" value = "<?php echo $_GET['edit_id']; ?>" /><?php
								}?>
								<div style="clear:left;">
									<span><input  id="frecl"  type="checkbox" name="SpecialCircumstances-Any" value="FRECL" <?php if(isset($pv['SpecialCircumstances'])){checked( $pv['SpecialCircumstances-Any'], "FRECL" );checked( $pv['SpecialCircumstances-Any'], "FRECL,POTSS,SSPAP" );} ?>></span><span>Foreclosure</span>
								</div>
								<div style="clear:left;">
									<span><input  type="checkbox" name="BedroomDescription" value="MASTR" <?php if(isset($pv['BedroomDescription'])) checked( $pv['BedroomDescription'], "MASTR" ); ?>></span><span>Master on Main</span>
								</div>
								<div style="clear:left;">
									<span><input  type="checkbox" name="BasementDesc-Any" value="BATH,BTDOR,BTHST,CRAWL,DAYLT,DRVWY,EXTEN,FINSH,FULL,INTER,PARTL,UNFIN" <?php if(isset($pv['BasementDesc-Any'])) checked( $pv['BasementDesc-Any'], "BATH,BTDOR,BTHST,CRAWL,DAYLT,DRVWY,EXTEN,FINSH,FULL,INTER,PARTL,UNFIN" ); ?>></span><span>Basement</span>
								</div>
								<div style="clear:left;">
									<span><input  type="checkbox" name="Stories" value="ONEST" <?php if(isset($pv['Stories'])) checked( $pv['Stories'], "ONEST" ); ?>></span><span>One Story</span>
								</div>
								<div style="clear:left;">
									<span><input  type="checkbox" name="SpecialCircumstances-All" value="RTCOM"></span><span>Adult Community</span>
								</div>
								<div style="clear:left;">
									<span><input  type="checkbox" name="WaterfrontFootage" value="1+" <?php if(isset($pv['WaterfrontFootage'])) checked( $pv['WaterfrontFootage'], "1+" ); ?>></span><span>Waterfront</span>
								</div>
								<div style="clear:left;">
									<span><input  type="checkbox" name="PoolonProperty-Any" value="ABOVE,FIBER,GUNIT,HEATD,INGRN,VINYL" <?php if(isset($pv['PoolonProperty-Any'])) checked( $pv['PoolonProperty-Any'], "ABOVE,FIBER,GUNIT,HEATD,INGRN,VINYL" ); ?>></span><span>Pool</span>
								</div>
								<div style="clear:left;">
									<span><input  type="checkbox" name="TennisonPropertyYN" value="1" <?php if(isset($pv['TennisonPropertyYN'])) checked( $pv['TennisonPropertyYN'], "1" ); ?>></span><span>Tennis Court</span>
								</div>
								<div style="clear:left;">
									</span><span>Square Footage Min:</span>
									<span><input type="text" name="SqFtTotal-From" value="<?php if(isset($pv['SqFtTotal-From'])) echo $pv['SqFtTotal-From'];?>">
								</div>
								<div style="clear:left;">
									</span><span>Square Footage Max:</span>
									<span><input type="text" name="SqFtTotal-UpTo" value="<?php if(isset($pv['SqFtTotal-UpTo'])) echo $pv['SqFtTotal-UpTo'];?>">
								</div>
								<div style="clear:left;"><span>Parking</span>
									<span>
										<select name="ParkingDesc-Any">
											<option value="" <?php if(! isset($pv['ParkingDesc-Any'])){echo 'selected';}else{selected( $pv['ParkingDesc-Any'], "" );}?>>Any</option>
											<option value="1GARG,2GARG,4GARG,GARGE" <?php selected( $pv['ParkingDesc-Any'], "GARG,1CARG,2CARG,3CARG,4CARG" ); ?>>Garage</option>
											<option value="CPORT,1CARP,2CARP" <?php selected( $pv['ParkingDesc-Any'], "CPORT,1CARP,2CARP" ); ?>>Carport</option>
											<option value="DRVWY,PRKPD" <?php selected( $pv['ParkingDesc-Any'], "DRVWY,PRKPD" ); ?>>Driveway</option>
											<option value="STRET" <?php selected( $pv['ParkingDesc-Any'], "STRET" ); ?>>Street</option>
										</select>
									</span>
								</div>
								
								<div style="clear:left;display:none">
									</span><span>Monthly HOA Fees Max:</span>
									<span><input  type="number" name="MonthlyAssocFee-UpTo" value="<?php if(isset($pv['MonthlyAssocFee-UpTo'])) echo $pv['MonthlyAssocFee-UpTo'];?>">
								</div>
								<div style="clear:left;display:none;"><span>Days on Market:</span>
									<span>
										<select name="aspk_days_on_market"  >
											<option value="" >Any</option>
											<option value="new_listings">New Listings</option>
											<option value="less_than_3_days">Less than 3 days</option>
											<option value="less_than_7_days">Less than 7 days</option>
											<option value="less_than_15_days">Less than 15 days</option>
											<option value="less_than_30_days">Less than 30 days</option>
											<option value="more_than_60_days,">More than 60 days</option>
											<option value="more_than_90_days,">More than 90 days</option>
											<option value="more_than_180_days,">More than 180 days</option>
										</select>
									</span>
								</div>
								<div style="clear:left;"><span>Price Reduced:</span>
									<span>
										<select name="PriceChangeTimestamp-From">
											<option value="">Any</option>
											<option value="<?php echo date('Y-m-d\TH:i:s', time()-259200);?>">In last 3 days</option>
											<option value="<?php echo date('Y-m-d\TH:i:s', time()-604800);?>">In last 7 days</option>
											<option value="<?php echo date('Y-m-d\TH:i:s', time()-1296000);?>">In last 15 days</option>
											<option value="<?php echo date('Y-m-d\TH:i:s', time()-2592000);?>">In last 30 days</option>
										</select>
									</span>
								</div>
								<div style="clear:left;"><span>Exclude Short Sales:</span>
									<span><input  id="exclude_ss" type="checkbox" name="SpecialCircumstances-Not" value="SSPAP,POTSS" <?php if(isset($pv['SpecialCircumstances-Not'])) checked( $pv['SpecialCircumstances-Not'], "SSPAP,POTSS" ); ?>></span>
								</div>
								<div style="clear:left;"><span>Elementary School:</span>
									<span>
										<?php
											$esstr = "[";
											sort($elementaryschool);
											foreach($elementaryschool as $es){
												if($esstr == "["){
													$esstr .=  '"'.$es->mls_id.'"';
												}else{
													$esstr .=  ',"'.$es->mls_id.'"';
												}
												
											}
											$esstr .= "]";
										?>
										<input id="elementaryschool" name="ElementarySchool" value="<?php if(isset($pv['ElementarySchool'])) echo $pv['ElementarySchool'];?>"/>
									</span>
								</div>
								<div style="clear:left;"><span>Middle School:</span>
									<span>
										<?php
											$msstr = "[";
											sort($middleschool);
											foreach($middleschool as $ms){
												if($msstr == "["){
													$msstr .=  '"'.$ms->mls_id.'"';
												}else{
													$msstr .=  ',"'.$ms->mls_id.'"';
												}
												
											}
											$msstr .= "]";
										?>
										<input id="middleschool" name="MiddleSchool" value="<?php if(isset($pv['MiddleSchool'])) echo $pv['MiddleSchool'];?>" />
									</span>
								</div>
								<div style="clear:left;"><span>High School:</span>
									<span>
										<?php
											$hsstr = "[";
											sort($highschool);
											foreach($highschool as $hs){
												if($hsstr == "["){
													$hsstr .=  '"'.$hs->mls_id.'"';
												}else{
													$hsstr .=  ',"'.$hs->mls_id.'"';
												}
												
											}
											$hsstr .= "]";
										?>
										<input id="highschool" name="HighSchool" value="<?php if(isset($pv['HighSchool'])) echo $pv['HighSchool'];?>" />
									</span>
								</div>
						</div>
				<!--adv ends-->
				
				<div><input type="hidden" name="Status-Any" value="A" /></div>
				<div><input id="advanced_open"type="hidden" name="advanced_open" value="<?php if($pv['advanced_open'] == '1'){echo '1';}else{echo '0';}?>" /></div>
				<?php if($scnt < 3){ ?>
					<div style="clear:left;"><input type="submit" class="btn btn-primary" name="aspk_search" value="Search"></div>
				<?php }else{?>
					<div style="clear:left;"><button onclick="tb_show('Sign up', url);return false;" class="btn btn-primary" >Search</button></div>
				<?php }?>
				<div style="clear:left;"><a href="<?php echo get_permalink();?>">Reset Search</a></div>
			</form>
				<div style="clear:left;margin-top:3em;">
					<input type="button" class="btn btn-primary basadv"  value="Advance Search" onclick="show_advance_fields()">
				</div>	
			<script>
				function show_advance_fields(){
					jQuery("#aspk_advance_search").toggle();
					if(jQuery("#advanced_open").val() == '0'){
						jQuery("#advanced_open").val('1');
					}else{
						jQuery("#advanced_open").val('0');
					}
					if(jQuery(".basadv").val() == "Advance Search"){
						jQuery(".basadv").val("Basic Search");
					}else{
						jQuery(".basadv").val("Advance Search");
					}
				}
				jQuery( document ).ready(function() {
					if(jQuery("#advanced_open").val() == '0'){
						jQuery("#aspk_advance_search").hide();
						jQuery(".basadv").val("Advance Search");
					}else{
						jQuery(".basadv").val("Basic Search");
					}
					jQuery('#shortsale').on('change', function() {
						if (jQuery('#shortsale').is(':checked')) {
							jQuery('#exclude_ss').prop('checked', false);
						}
					});
					jQuery('#exclude_ss').on('change', function() {
						if (jQuery('#exclude_ss').is(':checked')) {
							jQuery('#shortsale').prop('checked', false);
						}  
					});
					
					jQuery("#basic_form").submit(function(){
						if (jQuery('#frecl').is(':checked')) {
							if (jQuery('#shortsale').is(':checked')) {
								jQuery('#shortsale').attr('name', 'ShortSaleFC');
								jQuery('#frecl').val('FRECL,POTSS,SSPAP');
							}
						}
					});
					
					jQuery( "#elementaryschool" ).autocomplete({
						source: <?php echo $esstr;?>
					});
					jQuery( "#middleschool" ).autocomplete({
						source: <?php echo $msstr;?>
					});
					jQuery( "#highschool" ).autocomplete({
						source: <?php echo $hsstr;?>
					});
				});
			</script>
			<?php
		}
		
		function search_result_page(){
			
			$sc = new Aspk_Rets_Search();
			if(isset($_GET['srch_id'])){
				$sid = $_GET['srch_id'];
				$results = $sc->get_search_by_id($sid);
				foreach($results as $k=>$v){
					$_POST[$k] = $v;
				}
			}
			if(isset($_POST)) $_SESSION['aspk_search_fields'] = $_POST;
			$ctl = new Aspk_Rets_Wp_Controller();
			if(!$ctl->we_have_settings()){
				echo "Please save settings before proceeding";
				return;
			}
			
			$mid = null;
			$pg = null;
			if(isset($_GET['mid'])) $mid =  $_GET['mid'];
			if(isset($_GET['pg'])) $pg =  $_GET['pg'];
			$showpage = null;  //TODO there must be somewhere showpage=1
			
			if($mid && $pg){
				if($mid == session_id()){
					$showpage = $pg;
				}
			}
			
			$raw = $ctl->get_results($_POST, $showpage);
			
			if($ctl->get_last_error()){
				echo $ctl->get_last_error();
				return;
			}
			if($ctl->records_found() > 0){
				$total_pages = $ctl->get_total_pages();
				$current_page = $ctl->get_current_page();
				//session_start();
				$results = json_decode($raw);
				$all_results = $ctl->get_all_results($_POST);
				$_SESSION["all_result"] = json_decode($all_results);
			}else{
				echo "<div>No Data Found</div>";
				echo '<div><a href="'.get_permalink().'?ms=yes">Back to Search Page</a></div>';
				return;
			}
			
			$uid = get_current_user_id();
			if($uid != 0){
				$fav_properties = $sc->get_favorite_by_user($uid);
				if(!$fav_properties) $fav_properties = array();
			}else{
				$fav_properties = array();
			}
			
			?>
				<script>
					var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' );?>';
					function save_to_favourite(mls_id){
						var data = {
							'action'   : 'aspk_add_favourite',
							'mls_id' : mls_id,
					
						};
							
							jQuery.post(ajaxurl, data, function(response) {
							obj = JSON.parse(response);
							if(obj.st == 'ok'){
								jQuery('#mls_'+mls_id).html('<input onClick = "remove_to_favourite('+mls_id+')" type = "button" value = "Unsave"/>');
							}
						});
					}
					
					function remove_to_favourite(mls_id){
						var data = {
							'action'   : 'aspk_remove_favourite',
							'mls_id' : mls_id,
					
						};
							
							jQuery.post(ajaxurl, data, function(response) {
							obj = JSON.parse(response);
							if(obj.st == 'ok'){
								jQuery('#mls_'+mls_id).html('<input onClick = "save_to_favourite('+mls_id+')" type = "button" value = "Save to  Favorites"/>');
							}
						});
					}
					
					function hide_property(mls_id){
						var data = {
							'action'   : 'aspk_hide_property',
							'mls_id' : mls_id,
					
						};
							
							jQuery.post(ajaxurl, data, function(response) {
							obj = JSON.parse(response);
							if(obj.st == 'ok'){
								jQuery('#prop_'+mls_id).hide();
							}
						});
					}
					
					function save_search(){
						jQuery('#save_msg').hide();
						if(jQuery('#search_name').val().length < 3){
							alert('Please enter name of search to save');
							return false;
						}
						
						var checkboxarr=[];
						jQuery('input[type=checkbox]').each(function(){
							if(jQuery(this).is(':checked')){
								checkboxarr.push(jQuery(this).val());
							}
						});
						var x_search_id = jQuery('#x_srch_id').val();
						var data = {
							'action'   : 'aspk_save_search',
							'search_name' : jQuery('#search_name').val(),
							'x_search_id' : x_search_id,
							'cbarr' : checkboxarr
						};
						
						jQuery.post(ajaxurl, data, function(response) {
							obj = JSON.parse(response);
							if(obj.st == 'ok'){
								jQuery('#save_msg').html('Search has been saved');
								//alert(obj.sids);
								jQuery('#search_id').val(my_implode_js(',',obj.sids));
								jQuery('#save_msg').dialog();
							}else if(obj.st == 'er'){
								jQuery('#save_msg').html(obj.msg);
								jQuery('#search_id').val(my_implode_js(',',obj.sids));
								jQuery('#save_msg').dialog();
							}
							
						});
					}
					
					function my_implode_js(separator,array){
						   var temp = '';
						   for(var i=0;i<array.length;i++){
							   temp +=  array[i] 
							   if(i!=array.length-1){
									temp += separator  ; 
							   }
						   }//end of the for loop

						   return temp;
					}
					
					function notify_search(x){
						jQuery(x).attr('disabled','disabled');
						var search_id = jQuery("#search_id").val();
						if(search_id == ''){
							var edit_src_id;
							edit_src_id = jQuery("#x_srch_id").val();
						}
						var search_name = jQuery("#search_name").val();;
							var data = {
								'action'   : 'aspk_notify_search',
								'search_id_string' : search_id,
								'search_name' : search_name,
								'edit_search_id' : edit_src_id
							};
							jQuery.post(ajaxurl, data, function(response) {
								jQuery(x).removeAttr('disabled');
								obj = JSON.parse(response);
								if(obj.st == 'ok'){
									jQuery('#save_msg').html(obj.msg);
									jQuery('#save_msg').dialog();
									//jQuery("#search_id").val('');
								}else if(obj.st == 'er'){
									jQuery('#save_msg').html(obj.msg);
									jQuery('#save_msg').dialog();
								}	
							});
						
					}
				</script>
			<?php
			
			foreach($results as $r){
				if($sc->is_hidden($uid, $r->MLSNumber)) continue;
				
				$img = $ctl->get_images($r->Matrix_Unique_ID);

		?>
				<div style="clear:left;float:left;margin-bottom:1em;" id="prop_<?php echo $r->MLSNumber; ?>">
					<div style="float:left;"><div style="width:15em;">
					<?php if(isset($img[0]['Data'])){?>
						<a href="<?php echo get_permalink().'?propid='.$r->Matrix_Unique_ID.'&mls_id='.$r->MLSNumber.'&aspk_pg=y';?>"><img src="data:image/jpeg;base64,<?php echo base64_encode($img[0]['Data']);?>" width="400"/></a>
					<?php }else{ ?>
						<img src="<?php echo plugins_url( 'img/no_image.png', __FILE__ );?>" alt="No Image Available" width="400">
					<?php } ?>
					</div></div>
					<div style="clear:left;float:left;"><span class="t24-det-val t24-mls-StreetNumber"><a href="<?php echo get_permalink().'?propid='.$r->Matrix_Unique_ID.'&mls_id='.$r->MLSNumber.'&aspk_pg=y';?>"><?php echo $r->StreetNumber.'</span><span class="t24-det-val t24-mls-StreetDirPrefix"> '.$r->StreetDirPrefix.'</span><span class="t24-det-val t24-mls-StreetName"> '.$r->StreetName.'</span><span class="t24-det-val t24-mls-StreetSuffix"> '.$r->StreetSuffix.'</span><span class="t24-det-val t24-mls-StreetDirSuffix"> '.$r->StreetDirSuffix;?></a></span></div>
					<div style="clear:left;<?php if(empty($r->UnitNumber)) echo 'display:none;';?>"><span class="t24-det-name">Unit #</span><span class="t24-det-val t24-mls-UnitNumber"> <?php echo $r->UnitNumber;?></span></div>
					<div style="clear:left;margin-top:1em;"><span class="t24-det-val t24-mls-City"><?php echo $r->City.'</span><span class="t24-det-val t24-mls-PostalCode"> '.$r->PostalCode.'</span><span class="t24-det-val t24-mls-CountyOrParish"> '.$r->CountyOrParish.'</span> County';?></div>
					<!--<div><span class="t24-det-name">Status:</span><span class="t24-det-val t24-mls-Status"> <?php echo $r->Status;?></span></div>-->
					<div style="clear:left;"><span class="t24-det-name">Price:</span><span class="t24-det-val t24-mls-CurrentPrice"> <?php echo ' $'.number_format($r->CurrentPrice); ?></span></div>
					<div style="clear:left;"><span class="t24-det-name">Beds:</span> <?php echo $r->BedsTotal.'   Baths: '.$r->BathsFull.' / '.$r->BathsHalf; ?></span></div>
					<!--<div><span class="t24-det-name">Neighbourhood:</span><span class="t24-det-val t24-mls-SubdComplex"> <?php echo $r->SubdComplex;?></span></div>-->
					
					<div style="margin-top:2em;"><h2>Description:</h2></div>
					<div><span class="t24-det-val t24-mls-PublicRemarksConsumerView"><?php echo $r->PublicRemarksConsumerView;?></span></div>
					
					
					<script>
						var url = "<?php echo site_url();?>/wp-login.php?action=register&TB_iframe=true&width=400&height=500";
					</script>
					<?php 
					add_thickbox();
					if(!$uid){
							if(! in_array($r->MLSNumber,$fav_properties)){ ?>
								<div style = "float:left;padding: 1em;" id="mls_<?php echo $r->MLSNumber; ?>"><input onclick="tb_show('Sign up', url);return false;" type = "button" value = "Save to  Favorites"/></div>
							<?php }else{ ?>
								<div style = "float:left;padding: 1em;" >Favorite</div>
							<?php } ?>
							<div style = "float:left;padding: 1em;" ><input onclick="tb_show('Sign up', url);return false;" type = "button" value = "Hide"/></div>
					<?php }else{
						if(! in_array($r->MLSNumber,$fav_properties)){ ?>
								<div style = "float:left;padding: 1em;" id="mls_<?php echo $r->MLSNumber; ?>"><input onClick = "save_to_favourite(<?php echo $r->MLSNumber; ?>)" type = "button" value = "Save to  Favorites"/></div>
							<?php }else{ ?>
								<div style = "float:left;padding: 1em;" id="mls_<?php echo $r->MLSNumber; ?>" ><input onClick = "remove_to_favourite(<?php echo $r->MLSNumber; ?>)" type = "button" value = "Unsave"/></div>
							<?php } ?>
							<div style = "float:left;padding: 1em;" ><input onClick = "hide_property(<?php echo $r->MLSNumber; ?>)" type = "button" value = "Hide"/></div>
					<?php } ?>
					<div style="clear:both"><hr/></div>
				</div>
				
		<?php
			}//foreach ends
				$sc->show_save_search_form(get_current_user_id());
				$sc->notify_me();
				$this->share_search();
				echo '<div style="clear:left;float:left;"><a href="'.get_permalink().'?ms=yes">Back to Search Page</a></div>';
			?>
			<div style="clear:left;float:left;">Pages </div>
			<div style="float:left;">
		<?php
			
			for($pg = 1; $pg <= $total_pages && $pg <= 10;$pg++ ){
				if($pg == $current_page){
					echo $pg.'&nbsp;';
				}else{
					echo '<a href="'.get_permalink().'?mid='.session_id().'&pg='.$pg.'">'.$pg.'</a>&nbsp;';
				}
				
			}
			echo "</div";
		}
		
		function share_search(){ ?>
			<div style="float:left;clear:left;">
				<div style="float:left;"><h2>Share Search</h2></div>
			</div>
			<div style="float:left;clear:left;">
				<div style="float:left;width: 4em;">
					Email
				</div>
				<div style="float:left;">
					<input type="email" name="your_email" id="your_email" value=""/>
				</div>
			</div>
			<div style="float:left;clear:left;">
				<div style="float:left;width: 4em;">
					Name
				</div>
				<div style="float:left;">
					<input type="text" name="your_name" id="your_name" value=""/>
				</div>
			</div>
			<div style="float:left;clear:left;">
				<div style="margin-top:2em;">
					 <input type="button" onClick="share_search_ajax(this)" value="Share"/>
				</div>
			</div>
			<div id="save_share_serach_status"></div>
			<?php
			$this->share_search_script();
		}
		
		function share_search_script(){ ?>
			<script>
				var permalink = '<?php echo get_permalink(); ?>';
				function share_search_ajax(button){
					if(jQuery('#your_email').val() == ''){
						jQuery('#save_share_serach_status').text('Enter valid Email');
						jQuery('#save_share_serach_status').dialog();
						return false;
					}else{
						var your_email = jQuery('#your_email').val();
					}
					if(jQuery('#your_name').val() == ''){
						jQuery('#save_share_serach_status').text('Enter Your Name');
						jQuery('#save_share_serach_status').dialog();
						return false;
					}else{
						var your_name = jQuery('#your_name').val();
					}
					if(jQuery('#search_id').val() == ''){
						jQuery('#save_share_serach_status').text('Save Search First');
						jQuery('#save_share_serach_status').dialog();
						return false;
					}else{
						var search_id = jQuery('#search_id').val();
					}
					
					var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' );?>';
					var data = {
						'action' : 'aspk_share_saved_search',
						'permalink' : permalink,
						'your_email' : your_email,
						'your_name' : your_name,
						'search_id' : search_id
					};
					jQuery(button).attr('disabled','disabled');
					jQuery.post(ajaxurl, data, function(response) {
						jQuery(button).removeAttr('disabled');
						obj = JSON.parse(response);
						if(obj.st == 'ok'){
							jQuery('#save_share_serach_status').text(obj.msg);
							jQuery('#save_share_serach_status').dialog();
						}else if(obj.st == 'err'){
							jQuery('#save_share_serach_status').text(obj.msg);
							jQuery('#save_share_serach_status').dialog();
						}
					});
				}
				
			</script>
			<?php
		}
		
		
		function detail_page($muid){
			$ctl = new Aspk_Rets_Wp_Controller();
			if(!$ctl->we_have_settings()){
				echo "Please save settings before proceeding";
				return;
			}
			
						
			$raw = $ctl->get_property($muid);
			if($ctl->get_last_error()){
				echo $ctl->get_last_error();
				return;
			}
			
			if($ctl->records_found() > 0){
				
				$r = json_decode($raw);
				
			}else{
				echo "No Data Found";
				return;
			}
			
			$sc = new Aspk_Rets_Search();
			$uid = get_current_user_id();
			if($uid != 0){
				$fav_properties = $sc->get_favorite_by_user($uid);
				if(empty($fav_properties)) $fav_properties = array();
			}else{
				$fav_properties = array();
			}
			
			?>
				<script>

					var permalink = '<?php echo get_permalink(); ?>';
					function share_property(mls_id,button){
						if(jQuery('#search_email').val() == ''){
							alert('Enter valid Email');
							return false;
						}else{
							var search_email = jQuery('#search_email').val();
						}
						if(jQuery('#search_name').val() == ''){
							alert('Enter Your Name');
							return false;
						}else{
							var search_name = jQuery('#search_name').val();
						}
						
						var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' );?>';
						var data = {
							'action': 'aspk_share_propery_results',
							'recipient_email': search_email,
							'recipient_name': search_name,
							'mls_id': mls_id,
							'permalink': permalink
						};
						jQuery(button).attr('disabled','disabled');
						jQuery.post(ajaxurl, data, function(response) {
							jQuery(button).removeAttr('disabled');
							obj = JSON.parse(response);
							if(obj.st == 'ok'){
								alert(obj.msg);
							}else if(obj.st == 'err'){
								alert(obj.msg);
							}
						});  
					}
					
					jQuery(document).ready(function ($) {
						var options = {
							$FillMode: 4,
							$DragOrientation: 3,
							$ArrowNavigatorOptions: { 
								$Class: $JssorArrowNavigator$,
								$ChanceToShow: 2,   
								$AutoCenter: 0,  
								$Steps: 1                                       
							}
						};

						var jssor_slider1 = new $JssorSlider$("slider1_container", options);
						
					});
					
					var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' );?>';
					function save_to_favourite(mls_id){
						var data = {
							'action'   : 'aspk_add_favourite',
							'mls_id' : mls_id,
					
						};
							
							jQuery.post(ajaxurl, data, function(response) {
							obj = JSON.parse(response);
							if(obj.st == 'ok'){
								jQuery('#mls_'+mls_id).html('<input onClick = "remove_to_favourite('+mls_id+')" type = "button" value = "Unsave"/>');
							}
						});
					}
					
					function hide_property(mls_id){
						var data = {
							'action'   : 'aspk_hide_property',
							'mls_id' : mls_id,
					
						};
							
							jQuery.post(ajaxurl, data, function(response) {
							obj = JSON.parse(response);
							if(obj.st == 'ok'){
								javascript:history.back();
							}
						});
					}
					
					function remove_to_favourite(mls_id){
						var data = {
							'action'   : 'aspk_remove_favourite',
							'mls_id' : mls_id,
					
						};
							
							jQuery.post(ajaxurl, data, function(response) {
							obj = JSON.parse(response);
							if(obj.st == 'ok'){
								jQuery('#mls_'+mls_id).html('<input onClick = "save_to_favourite('+mls_id+')" type = "button" value = "Save to  Favorites"/>');
							}
						});
					}
				</script>
				
				<?php $imgs = $ctl->get_images($muid); ?>
					
				<div id="slider1_container" style="position: relative; top: 0px; left: 0px; width: 800px;
					height: 600px;">
					<!-- Slides Container -->
					<div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 800px; height: 600px;
						overflow: hidden;">
						<?php if(count($imgs) > 0){
								$fimg = true;
							?>
							<?php foreach($imgs as $img){
									if($fimg){
										$fimg = false;
										continue;
									}
							?>
								<div><img u="image" src="data:image/jpeg;base64,<?php echo base64_encode($img['Data']);?>" /></div>
							<?php } ?>
						<?php }else{ ?>
							<img src="<?php echo plugins_url( 'img/no_image.png', __FILE__ );?>" alt="No Image Available" width="400">
						<?php } ?>
					</div>
					<!-- Arrow Navigator Skin Begin -->
					<style>
						/* jssor slider arrow navigator skin 03 css */
						/*
						.jssora03l              (normal)
						.jssora03r              (normal)
						.jssora03l:hover        (normal mouseover)
						.jssora03r:hover        (normal mouseover)
						.jssora03ldn            (mousedown)
						.jssora03rdn            (mousedown)
						*/
						.jssora03l, .jssora03r, .jssora03ldn, .jssora03rdn
						{
							position: absolute;
							cursor: pointer;
							display: block;
							background: url(<?php echo plugins_url( 'img/a03.png', __FILE__ );?>) no-repeat;
							overflow:hidden;
						}
						.jssora03l { background-position: -3px -33px; }
						.jssora03r { background-position: -63px -33px; }
						.jssora03l:hover { background-position: -123px -33px; }
						.jssora03r:hover { background-position: -183px -33px; }
						.jssora03ldn { background-position: -243px -33px; }
						.jssora03rdn { background-position: -303px -33px; }
						
						#map_canvas img {
							max-width: none !important;
						}
					</style>
					<!-- Arrow Left -->
					<span u="arrowleft" class="jssora03l" style="width: 55px; height: 55px; top: 250px; left: 8px;">
					</span>
					<!-- Arrow Right -->
					<span u="arrowright" class="jssora03r" style="width: 55px; height: 55px; top: 250px; right: 8px">
					</span>
					<!-- Arrow Navigator Skin End -->
				</div>
		
				<div style="clear:left;float:left;">
					
					<div style="float:left;"><span class="t24-det-val t24-mls-StreetNumber"><?php echo $r->StreetNumber.'</span><span class="t24-det-val t24-mls-StreetDirPrefix"> '.$r->StreetDirPrefix.'</span><span class="t24-det-val t24-mls-StreetName"> '.$r->StreetName.'</span><span class="t24-det-val t24-mls-StreetSuffix"> '.$r->StreetSuffix.'</span><span class="t24-det-val t24-mls-StreetDirSuffix"> '.$r->StreetDirSuffix;?></span></div>
						<div style="clear:left;<?php if(empty($r->UnitNumber)) echo 'display:none;';?>"><span class="t24-det-name">Unit #</span><span class="t24-det-val t24-mls-UnitNumber"> <?php echo $r->UnitNumber;?></span></div>
						<div style="clear:left;margin-top:1em;"><span class="t24-det-val t24-mls-City"><?php echo $r->City.'</span><span class="t24-det-val t24-mls-PostalCode"> '.$r->PostalCode.'</span><span class="t24-det-val t24-mls-CountyOrParish"> '.$r->CountyOrParish.'</span> County';?></div>
						<div style="clear:left;"><span class="t24-det-name">Status:</span><span class="t24-det-val t24-mls-Status"> <?php echo $r->Status;?></span></div>
						<div style="clear:left;"><span class="t24-det-name">Price:</span><span class="t24-det-val t24-mls-CurrentPrice"> <?php echo ' $'.number_format($r->CurrentPrice); ?></span></div>
						<div style="clear:left;"><span class="t24-det-name">Property Type:</span><span class="t24-det-val t24-mls-PropertyType"> <?php echo $r->PropertyType; ?></span></div>
						<div style="clear:left;"><span class="t24-det-name">Beds:</span> <?php echo $r->BedsTotal.'   Baths: '.$r->BathsFull.' / '.$r->BathsHalf; ?></span></div>
						<div style="clear:left;"><span class="t24-det-name">Neighbourhood:</span><span class="t24-det-val t24-mls-SubdComplex"> <?php echo $r->SubdComplex;?></span></div>
						
						<div style="margin-top:2em;"><h2>Description:</h2></div>
						<div><span class="t24-det-val t24-mls-PublicRemarksConsumerView"><?php echo $r->PublicRemarksConsumerView;?></span></div>
						
						<div style="margin-top:2em;"><h2>Directions:</h2></div>
						<div style="clear:left;">
							 <script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
								<script>
								  var geocoder;
								  var map;
								  var mapOptions = {
									  zoom: 14,
									  zoomControl: true,
									  mapTypeId: google.maps.MapTypeId.ROADMAP
									}
								  var marker;

								  jQuery(document).ready(function ($) {
										geocoder = new google.maps.Geocoder();
										map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
										codeAddress();
								  });

								  function codeAddress() {
									var address = "<?php echo $r->StreetNumber.' '.$r->StreetDirPrefix.' '.$r->StreetName.' '.$r->StreetSuffix.' '.$r->StreetDirSuffix .' '.$r->City,', GA, '.$r->PostalCode ?>";
									geocoder.geocode( { 'address': address}, function(results, status) {
									  if (status == google.maps.GeocoderStatus.OK) {
										map.setCenter(results[0].geometry.location);
										if(marker)
										  marker.setMap(null);
										marker = new google.maps.Marker({
											map: map,
											position: results[0].geometry.location,
											draggable: true
										});
									  } else {
										alert('Geocode was not successful for the following reason: ' + status);
									  }
									});
								  }
								</script>
						</div>
						<div id="map_canvas" style="height:25em;margin:0px;padding:0px;"></div>
						<div><span class="t24-det-val t24-mls-Directions"><?php echo $r->Directions;?></span></div>
						
						<div style="margin-top:2em;"><h2>Interior Features:</h2></div>
						<div><span class="t24-det-name">Approx Sq Ft:</span><span class="t24-det-val t24-mls-SqFtTotal"> <?php echo $r->SqFtTotal.' Source: '.$r->SqFtSource;?></span></div>
						<div><span class="t24-det-name">Interior:</span><span class="t24-det-val t24-mls-Interior"> <?php echo $r->Interior;?></span></div>
						<div><span class="t24-det-name">Rooms:</span><span class="t24-det-val t24-mls-RoomsDescription"> <?php echo $r->RoomsDescription;?></span></div>
						<div><span class="t24-det-name">Bedroom Description:</span><span class="t24-det-val t24-mls-BedroomDescription"> <?php echo $r->BedroomDescription;?></span></div>
						<div><span class="t24-det-name">Master Bath Description:</span><span class="t24-det-val t24-mls-MasterBathFeatures"> <?php echo $r->MasterBathFeatures;?></span></div>
						<div><span class="t24-det-name">Kitchen Description:</span><span class="t24-det-val t24-mls-KitchenFeatures"> <?php echo $r->KitchenFeatures;?></span></div>
						<div><span class="t24-det-name">Dining Room Desc:</span><span class="t24-det-val t24-mls-DiningRoomDescription"> <?php echo $r->DiningRoomDescription;?></span></div>
						<div><span class="t24-det-name">Laundry:</span><span class="t24-det-val t24-mls-LaundryFeaturesLocation"> <?php echo $r->LaundryFeaturesLocation;?></span></div>
						
						<div><span class="t24-det-name">Basement:</span><span class="t24-det-val t24-mls-BasementDesc"> <?php echo $r->BasementDesc;?></span></div>
						<div><span class="t24-det-name">Accessibility Features:</span><span class="t24-det-val t24-mls-DisabilityAccess"> <?php echo $r->DisabilityAccess;?></span></div>
						<div><span class="t24-det-name">Number of Fireplaces:</span><span class="t24-det-val t24-mls-FireplacesNum"> <?php echo $r->FireplacesNum;?></span></div>
						<div><span class="t24-det-name">Cooling:</span><span class="t24-det-val t24-mls-CoolingDescription"> <?php echo $r->CoolingDescription;?></span></div>
						<div><span class="t24-det-name">Heating:</span><span class="t24-det-val t24-mls-HeatType"> <?php echo $r->HeatType;?></span></div>
						<div><span class="t24-det-name">Appliances:</span><span class="t24-det-val t24-mls-ApplianceDesc"> <?php echo $r->ApplianceDesc;?></span></div>
						
						<div style="margin-top:2em;"><h2>Exterior Features:</h2></div>
						<div><span class="t24-det-name">Year Built:</span><span class="t24-det-val t24-mls-YearBuilt"> <?php echo $r->YearBuilt;?></span></div>
						<div><span class="t24-det-name">Style:</span><span class="t24-det-val t24-mls-Style"> <?php echo $r->Style;?></span></div>
						<div><span class="t24-det-name">Stories:</span><span class="t24-det-val t24-mls-Stories"> <?php echo $r->Stories;?></span></div>
						<div><span class="t24-det-name">Levels:</span><span class="t24-det-val t24-mls-Levels"> <?php echo $r->UnitLevels;?></span></div>
						<div><span class="t24-det-name">Construction:</span><span class="t24-det-val t24-mls-ConstructionDesc"> <?php echo $r->ConstructionDesc;?></span></div>
						<div><span class="t24-det-name">Roof:</span><span class="t24-det-val t24-mls-RoofType"> <?php echo $r->RoofType;?></span></div>
						<div><span class="t24-det-name">Exterior:</span><span class="t24-det-val t24-mls-Exterior"> <?php echo $r->Exterior;?></span></div>
						<div><span class="t24-det-name">Water:</span><span class="t24-det-val t24-mls-WaterSource"> <?php echo $r->WaterSource;?></span></div>
						<div><span class="t24-det-name">Sewer:</span><span class="t24-det-val t24-mls-SewerDesc"> <?php echo $r->SewerDesc;?></span></div>
						<div><span class="t24-det-name">Parking:</span><span class="t24-det-val t24-mls-ParkingDesc"> <?php echo $r->ParkingDesc;?></span></div>
						<div><span class="t24-det-name">Lot Desc:</span><span class="t24-det-val t24-mls-LotDesc"><?php echo $r->LotDesc;?></span></div>
						<div><span class="t24-det-name">Lot Size:</span><span class="t24-det-val t24-mls-ApproximateLotSize"> <?php echo $r->ApproximateLotSize;?></span></div>
						<div><span class="t24-det-name">Water Front Footage:</span><span class="t24-det-val t24-mls-WaterfrontFootage"> <?php echo $r->WaterfrontFootage;?></span></div>
						
						<div style="margin-top:2em;"><h2>Schools:</h2></div>
						<div><span class="t24-det-name">Elementary:</span><span class="t24-det-val t24-mls-ElementarySchool"> <?php echo $r->ElementarySchool;?></span></div>
						<div><span class="t24-det-name">Middle:</span><span class="t24-det-val t24-mls-MiddleSchool"> <?php echo $r->MiddleSchool;?></span></div>
						<div><span class="t24-det-name">High School:</span><span class="t24-det-val t24-mls-HighSchool"> <?php echo $r->HighSchool;?></span></div>
						
						<div style="margin-top:2em;"><h2>Neighbourhood Info:</h2></div>
						<div><span class="t24-det-name">Amenities:</span><span class="t24-det-val t24-mls-NeighborhoodAmenities"> <?php echo $r->NeighborhoodAmenities;?></span></div>
						<div><span class="t24-det-name">HOA Monthly Fee:</span><span class="t24-det-val t24-mls-MonthlyAssocFee"> <?php echo $r->MonthlyAssocFee;?></span></div>
						<div><span class="t24-det-name">HOA Annual Fee:</span><span class="t24-det-val t24-mls-AnnualAssocFee"> <?php echo $r->AnnualAssocFee;?></span></div>
						<div><span class="t24-det-name">Fee Frequency:</span><span class="t24-det-val t24-mls-AssocFeeDesc"> <?php echo $r->AssocFeeDesc;?></span></div>
						<div><span class="t24-det-name">Fee Includes:</span><span class="t24-det-val t24-mls-AssocFeeIncludes"> <?php echo $r->AssocFeeIncludes;?></span></div>
						<div style="margin-top:1em;"><span class="t24-det-name">Master Assoc Annual Fee:</span><span class="t24-det-val t24-mls-AnnualMasterAssocFee"> <?php echo $r->AnnualMasterAssocFee;?></span></div>
						<div><span class="t24-det-name">Master Assoc Fee Desc:</span><span class="t24-det-val t24-mls-AnnualMasterAssocFeeDesc"> <?php echo $r->AnnualMasterAssocFeeDesc;?></span></div>
						
						<div style="margin-top:2em;"><h2>Taxes:</h2></div>
						<div><span class="t24-det-name">Tax Amount:</span><span class="t24-det-val t24-mls-Taxes"> <?php echo $r->Taxes;?></span></div>
						<div><span class="t24-det-name">Tax Year:</span><span class="t24-det-val t24-mls-TaxYear"> <?php echo $r->TaxYear;?></span></div>
						
						<div style="margin-top:2em;"><h2>Special Circumstances:</h2></div>
						<div><span class="t24-det-val t24-mls-SpecialCircumstances"><?php echo $r->SpecialCircumstances;?></span></div>
						
						<div style="margin-top:2em;"><h2>MLS Info:</h2></div>
						<div><span class="t24-det-name">Status:</span><span class="t24-det-val t24-mls-Status"> <?php echo $r->Status;?></span></div>
						<div><span class="t24-det-name">FMLS #:</span><span class="t24-det-val t24-mls-MLSNumber"> <?php echo $r->MLSNumber;?></span></div>
						<div><span class="t24-det-name">Area:</span><span class="t24-det-val t24-mls-SearchArea"> <?php echo $r->SearchArea;?></span></div>
						
						<div style="margin-top:1em;"><span class="t24-det-name">Listing Provided Courtesy Of:</span><span class="t24-det-val t24-mls-ListOfficeName"> <?php echo $r->ListOfficeName;?></span></div>
						
						<div style="margin-top:2em;"><h2>Disclaimer:</h2></div>
						
						<?php
						$hidden_properties = $sc->check_if_property_is_hidden($uid,$r->MLSNumber);
						if($uid > 0){
							if(! in_array($r->MLSNumber,$fav_properties)){ ?>
								<div style = "float:left;padding: 1em;" id="mls_<?php echo $r->MLSNumber; ?>"><input onClick = "save_to_favourite(<?php echo $r->MLSNumber; ?>)" type = "button" value = "Save to  Favorites"/></div>
							<?php }else{ ?>
								<div style = "float:left;padding: 1em;" id="mls_<?php echo $r->MLSNumber; ?>" ><input onClick = "remove_to_favourite(<?php echo $r->MLSNumber; ?>)" type = "button" value = "Unsave"/></div>
							<?php }
							if($hidden_properties == true){ ?>
								<div style = "float:left;padding: 1em;" ><input onClick = "unhide_property(<?php echo $r->MLSNumber; ?>)" type = "button" value = "Unhide"/></div> <?php
							}else{ ?>
								<div style = "float:left;padding: 1em;" ><input onClick = "hide_property(<?php echo $r->MLSNumber; ?>)" type = "button" value = "Hide"/></div>
							<?php } ?>
						<?php } //uid?>
						
						<div style="float:left;clear:left;margin-top:2em;">
							<div style="float:left;">
								<input type="button" value="Request Information" id="request_information"/>
							</div>
						</div>
						<div style="float:left;clear:left;margin-top:2em;">
							<div style="float:left;">
								<input type="button" value="Schedule Showing" id="schedule_showing"/>
							</div>
						</div>
						<div style="float:left;clear:left;">
							<div style="float:left;"><h2>Share Search Results:</h2></div>
						</div>
						<div style="float:left;clear:left;">
							<div style="float:left;width: 4em;">
								Email
							</div>
							<div style="float:left;">
								<input type="email" name="search_email" id="search_email" value=""/>
							</div>
						</div>
						<div style="float:left;clear:left;">
							<div style="float:left;width: 4em;">
								Name
							</div>
							<div style="float:left;">
								<input type="text" name="search_name" id="search_name" value=""/>
							</div>
						</div>
						<div style="float:left;clear:left;">
							<div style="margin-top:2em;">
								 <input type="button" id="search_name" onClick="share_property(<?php echo $r->MLSNumber; ?>,this)" value="Share"/>
							</div>
						</div>
				</div>
				<script>
				function unhide_property(mls_id){
					var data = {
						'action'   : 'aspk_unhide_property',
						'mls_id' : mls_id,
				
					};
						
						jQuery.post(ajaxurl, data, function(response) {
						obj = JSON.parse(response);
						if(obj.st == 'ok'){
							window.location.href = '<?php echo get_permalink().'?mls_id='; ?>'+mls_id;
						}
					});
				}
				</script>
				
		<?php
			if(isset($_GET['aspk_pg'])){
				$pg_result = $_SESSION["all_result"];
			}else{
				$pg_result = null;
			}
			

			$img_gid = $_GET['propid'];
			$n = 0;
			
			if($pg_result){
				foreach($pg_result as $k => $value){
					
					if($value->Matrix_Unique_ID == $img_gid){
						break;
					}
					$n++;
				}
			}
			
			?><div class="row">
				 
			<?php if($pg_result) $this->show_pagination($n, $pg_result);?>
			
				<div><a href="<?php echo get_permalink()?>?ms=yes">Back to Search Page</a></div>
			</div><?php
			
		}
		
		function show_pagination($n, &$pg_result){
			
			if($n != 0){
			?>
				<div><a href="<?php echo get_permalink().'?propid='.$pg_result[$n-1]->Matrix_Unique_ID.'&mls_id='.$pg_result[$n-1]->MLSNumber.'&aspk_pg=y';?>">Previous</a></div>
			<?php 
			}
			
			if(true){
			?>
				<div><a href="<?php echo get_permalink().'?propid='.$pg_result[$n+1]->Matrix_Unique_ID.'&mls_id='.$pg_result[$n+1]->MLSNumber.'&aspk_pg=y';?>">Next</a></div>
			<?php 
			}
		}
			
		
		
		
		function share_propery_script(){ ?>
			<script>
				var permalink = '<?php echo get_permalink(); ?>';
				function share_property(mls_id,button){
					if(jQuery('#search_email').val() == ''){
						alert('Enter valid Email');
						return false;
					}else{
						var search_email = jQuery('#search_email').val();
					}
					if(jQuery('#search_name').val() == ''){
						alert('Enter Your Name');
						return false;
					}else{
						var search_name = jQuery('#search_name').val();
					}
					
					var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' );?>';
					var data = {
						'action': 'aspk_share_propery_results',
						'recipient_email': search_email,
						'recipient_name': search_name,
						'mls_id': mls_id,
						'permalink': permalink
					};
					jQuery(button).attr('disabled','disabled');
					jQuery.post(ajaxurl, data, function(response) {
						jQuery(button).removeAttr('disabled');
						obj = JSON.parse(response);
						if(obj.st == 'ok'){
							alert(obj.msg);
						}else if(obj.st == 'err'){
							alert(obj.msg);
						}
					});  
				}
				
			</script>
			<?php
		}
		
		function aspk_search_del(){
			$s = new Aspk_Rets_Search();
			$s->search_delete($_POST['sid']);
			$ret = array('st' => 'ok','msg'=>'Search Deleted');
			echo json_encode($ret);
			exit;
		}
		
	}
}

	
new Agile_Rets_Aspk();
